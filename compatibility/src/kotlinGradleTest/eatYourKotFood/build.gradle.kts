import org.ysb33r.gradle.gradletest.GradleTestSet

plugins {
    `java-gradle-plugin`
    id("org.ysb33r.gradletest")
    groovy
}

group = "my"
version = "1.0"

System.getProperty("OFFLINE_REPO_DSL")
apply(from=System.getProperty("OFFLINE_REPO_DSL"))

gradleTestSets {
    testSets {
        val main by getting(GradleTestSet::class) {
            versions ("7.6.1","8.0.2")
        }
    }
}

tasks {
    val runGradleTest by creating {
        dependsOn ("gradleTest")
    }
}

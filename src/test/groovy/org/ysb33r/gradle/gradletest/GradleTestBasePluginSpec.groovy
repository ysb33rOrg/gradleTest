/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.ysb33r.gradle.gradletest.testfixtures.UnitTestSpecification

class GradleTestBasePluginSpec extends UnitTestSpecification {

    GradleTestSetExtension testSetExtension

    void setup() {
        project.pluginManager.apply(GradleTestBasePlugin)
        testSetExtension = project.extensions.getByType(GradleTestSetExtension)
    }

    void 'Create the extension and add one testset'() {
        setup:
        project.allprojects {
            gradleTestSets {
                testSets {
                    myTest {

                    }
                }
            }
        }

        expect:
        testSetExtension.testSets.size() == 1
        project.tasks.getByName('myTestGradleTest')
    }
}
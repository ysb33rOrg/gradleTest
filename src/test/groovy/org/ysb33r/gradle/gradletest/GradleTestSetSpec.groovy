/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.gradle.api.tasks.SourceSetContainer
import org.ysb33r.gradle.gradletest.testfixtures.UnitTestSpecification
import spock.lang.Issue

import static org.ysb33r.gradle.gradletest.GradleTestSet.DEFAULT_DEPRECATION_WARNINGS
import static org.ysb33r.gradle.gradletest.internal.TestSet.baseName
import static org.ysb33r.gradle.gradletest.internal.TestSet.getGeneratorTaskName

class GradleTestSetSpec extends UnitTestSpecification {

    GradleTestSet setUnderTest

    void setup() {
        project.pluginManager.apply(GradleTestBasePlugin)
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        versions '7.3', '8.2', '8.9'
                    }
                }
            }
        }

        setUnderTest = project.extensions.getByType(GradleTestSetExtension).testSets.getByName('mine')
    }

    void 'Tasks are generated'() {
        setup:
        final basename = baseName('mine')

        expect:
        project.tasks.getByName(basename)
        project.tasks.getByName(getGeneratorTaskName(basename))
    }

    void 'Can configure versions'() {
        expect:
        setUnderTest.versions.get().containsAll(['7.3', '8.2', '8.9'])
    }

    void 'No deprecation messages if global turn-off'() {
        setup:
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        deprecationMessagesAreFailuresForAllVersions = false
                    }
                }
            }
        }

        expect:
        setUnderTest.deprecationMessagesAreFailures.get().versionMap.keySet().containsAll(['7.3', '8.2', '8.9'])
        setUnderTest.deprecationMessagesAreFailures.get().versionMap.values()*.empty
    }

    void 'Deprecation messages for all versions if global turn-on'() {
        setup:
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        deprecationMessagesAreFailuresForAllVersions = true
                    }
                }
            }
        }

        expect:
        setUnderTest.deprecationMessagesAreFailures.get().versionMap.values()*.size()*.every {
            it == DEFAULT_DEPRECATION_WARNINGS.size()
        }
    }

    void 'Can configure deprecation messages for a specific version'() {
        setup:
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        deprecationMessagesAreFailures = { ver -> ver.startsWith('8') }
                        deprecationMessageChecksForVersion('8.9', 'foo')
                        defaultDeprecationMessageChecks.remove(0)
                    }
                }
            }
        }
        final result = setUnderTest.deprecationMessagesAreFailures.get().versionMap

        expect:
        result['7.3'].empty
        result['8.2'].size() == DEFAULT_DEPRECATION_WARNINGS.size() - 1
        result['8.9'].size() == 1
    }

    void 'Source set contains the generated directory'() {
        setup:
        final ss = project.extensions.getByType(SourceSetContainer).getByName('mineGradleTest')
        final dirs = ss.allSource.sourceDirectories

        expect:
        ss.java.sourceDirectories.files.empty
        ss.resources.sourceDirectories.files.empty
        !ss.groovy.sourceDirectories.files.empty
        dirs.files.find { it.absolutePath.contains('mineGradleTest') }
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/gradleTest/-/issues/160')
    void 'Augmenting message checks should not emit null pointer exceptions'() {
        when:
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        augmentMessageChecksForVersionFromDefault('8.12.1', 'some pattern')
                        augmentMessageChecksForVersionFromDefault('8.11.1', ['some pattern'])
                    }
                }
            }
        }

        then:
        noExceptionThrown()
    }
}
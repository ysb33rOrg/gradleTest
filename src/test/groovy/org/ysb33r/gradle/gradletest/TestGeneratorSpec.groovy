/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.gradle.testkit.runner.TaskOutcome
import org.ysb33r.gradle.gradletest.internal.TestSet
import org.ysb33r.gradle.gradletest.testfixtures.SimpleIntegrationSpecification
import spock.lang.Issue

class TestGeneratorSpec extends SimpleIntegrationSpecification {
    public static final String TESTSET = 'goFundMe'
    public static final String TEST_TASK = TestSet.baseName(TESTSET)

    void 'Can generate sources'() {
        setup:
        writeBuildFile()
        createGradleTestSources()
        final taskName = TestSet.getGeneratorTaskName(TEST_TASK)

        when:
        final result = getGradleRunner([taskName, '-s']).build()

        then:
        result.task(":${taskName}").outcome == TaskOutcome.SUCCESS
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/gradleTest/-/issues/161')
    void 'Backslashes are escaped correctly'() {
        final tf89 = new File(projectDir, '.generated-src/gradleTestPlugin/goFundMeGradleTest/src/groovy/test/ysb33r/gradle/compatibility/gofundmegradletest/TestA__8_9Spec.groovy')
        writeBuildFile()
        createGradleTestSources()
        buildFile << """
        gradleTestSets {
            testSets {
                ${TESTSET} {
                    defaultDeprecationMessageChecks = [
                        /Some pattern matching dot character: \\./,
                        /Some pattern matching any whitespace character: \\s/,
                    ]
                }
            }
        }
        """.stripIndent()
        final taskName = TestSet.getGeneratorTaskName(TEST_TASK)

        when:
        final result = getGradleRunner([taskName, '-s']).build()

        then:
        result.task(":${taskName}").outcome == TaskOutcome.SUCCESS
        tf89.text.contains('\\\\s')
    }

    void 'When there is no gradleTest folder, the task should not fail, just be skipped'() {
        setup:
        writeBuildFile()
        final taskName = TestSet.getGeneratorTaskName(TEST_TASK)

        when:
        final result = getGradleRunner([taskName, '-s']).build()

        then:
        result.task(":${taskName}").outcome == TaskOutcome.SKIPPED
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/gradleTest/-/issues/159')

    void 'Old test cases are removed'() {
        setup:
        final tf89 = new File(projectDir, '.generated-src/gradleTestPlugin/goFundMeGradleTest/src/groovy/test/ysb33r/gradle/compatibility/gofundmegradletest/TestA__8_9Spec.groovy')
        final tf811 = new File(projectDir, '.generated-src/gradleTestPlugin/goFundMeGradleTest/src/groovy/test/ysb33r/gradle/compatibility/gofundmegradletest/TestA__8_11_1Spec.groovy')
        final pd89 = new File(buildDir, 'goFundMeGradleTest/8.9/testA')
        final pd89kts = new File(buildDir, 'goFundMeGradleTest/8.9/testA_kts')
        final pd811 = new File(buildDir, 'goFundMeGradleTest/8.11.1/testA')
        final resourceFile = new File(buildDir, 'goFundMeGradleTest/7.3.3/testA_kts/resource.txt')

        writeBuildFile()
        createGradleTestSources()
        final taskName = TestSet.getGeneratorTaskName(TEST_TASK)

        when:
        final result = getGradleRunner([taskName, '-s']).build()

        then:
        result.task(":${taskName}").outcome == TaskOutcome.SUCCESS
        tf89.exists()
        pd89.exists()
        resourceFile.exists()

        when:
        removeGroovyTestSources()
        final result2 = getGradleRunner([taskName, '-s']).build()

        then:
        result2.task(":${taskName}").outcome == TaskOutcome.SUCCESS
        !tf89.exists()
        pd89.exists() // not removing.
        pd89kts.exists()

        when:
        writeBuildFile(['7.3.3', '8.9'])
        createGradleTestSources()
        final result3 = getGradleRunner([taskName, '-s']).build()

        then:
        result3.task(":${taskName}").outcome == TaskOutcome.SUCCESS
        tf89.exists()
        pd89.exists()
        !tf811.exists()
        pd811.exists() // we do not remove old project files, as we might need them for inspection

        when:
        removeTestResourceFile()
        final result4 = getGradleRunner([taskName, '-s']).build()

        then:
        result4.task(":${taskName}").outcome == TaskOutcome.SUCCESS
        !resourceFile.exists()
    }

    void writeBuildFile(List<String> testVersions = ['7.3.3', '8.9', '8.11.1']) {
        final String testVersionsDSL = testVersions.collect { "'${it}'" }.join(', ')
        buildFile.text = """
        plugins {
            id 'org.ysb33r.gradletest.base'
        }
        
        gradleTestSets {
            testSets {
                ${TESTSET} {
                    versions ${testVersionsDSL}
                }
            }
        }
        """.stripIndent()

        settingsFile.text = "rootProject.name = 'test-generator'"
    }

    void createGradleTestSources() {
        final srcDir = new File(projectDir, TEST_SOURCES_SUBDIR)
        srcDir.mkdirs()
        new File(srcDir, 'build.gradle').text = ''
        new File(srcDir, 'build.gradle.kts').text = ''
        new File(srcDir, 'staged.foo.build.gradle.kts').text = ''
        new File(srcDir, 'settings.gradle').text = ''
        new File(srcDir, 'gradle.properties').text = ''
        new File(srcDir, 'resource.txt').text = ''
    }

    void removeGroovyTestSources() {
        final srcDir = new File(projectDir, TEST_SOURCES_SUBDIR)
        new File(srcDir, 'build.gradle').delete()
    }

    void removeTestResourceFile() {
        final srcDir = new File(projectDir, TEST_SOURCES_SUBDIR)
        new File(srcDir, 'resource.txt').delete()
    }

    private static final String TEST_SOURCES_SUBDIR = 'src/goFundMeGradleTest/testA'
}
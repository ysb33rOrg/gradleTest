/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.testing.Test
import org.ysb33r.gradle.gradletest.testfixtures.UnitTestSpecification

class GradleTestPluginSpec extends UnitTestSpecification {

    def "Applying the plugin"() {
        when: "The plugin is applied"
        project.pluginManager.apply(GradleTestPlugin)

        then: "The test task is created"
        project.tasks.getByName('gradleTest') instanceof Test

        and: "The generator task is created"
        project.tasks.getByName('gradleTestGenerator') instanceof TestGenerator

        and: "A compile task is created"
        project.tasks.getByName('compileGradleTestGroovy') instanceof GroovyCompile

        and: "A compile configuration is added"
        project.configurations.findByName('gradleTestCompileClasspath') != null
    }

}


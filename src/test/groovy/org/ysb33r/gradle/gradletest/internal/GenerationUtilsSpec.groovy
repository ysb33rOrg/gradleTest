/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import org.ysb33r.gradle.gradletest.BasicTestDefinition
import org.ysb33r.gradle.gradletest.FullTestDefinition
import org.ysb33r.gradle.gradletest.GradleTestBasePlugin
import org.ysb33r.gradle.gradletest.GradleTestSet
import org.ysb33r.gradle.gradletest.GradleTestSetExtension
import org.ysb33r.gradle.gradletest.testfixtures.UnitTestSpecification
import org.ysb33r.grolifant5.api.core.FileSystemOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import spock.lang.TempDir
import spock.lang.Unroll

import java.nio.file.Files

class GenerationUtilsSpec extends UnitTestSpecification {

    @TempDir
    File targetDir

    GradleTestSet setUnderTest
    FileSystemOperations fsOperations
    File srcOutputDir
    File templateDir
    File projectSourceDir

    void setup() {
        project.pluginManager.apply(GradleTestBasePlugin)
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        versions '7.3', '8.2', '8.9'
                    }
                }
            }
        }

        setUnderTest = project.extensions.getByType(GradleTestSetExtension).testSets.getByName('mine')
        fsOperations = ProjectOperations.find(project).fsOperations
        srcOutputDir = new File(targetDir, 'generated')
        templateDir = new File(targetDir, 'templates')
        projectSourceDir = new File(targetDir, 'src/gradleTest/testA')

        srcOutputDir.mkdirs()
        templateDir.mkdirs()
        projectSourceDir.mkdirs()
    }

    void 'Can generate test source files from template'() {
        setup:
        final gv = '8.2'
        final bf = 'build.gradle'

        when:
        GenerationUtils.generateTest(
            srcOutputDir,
            loadTemplateFile(),
            fsOperations,
            testDefinition(gv, bf)
        )

        final files = new File(srcOutputDir, 'test/foo').listFiles()

        then:
        files.size() == 1
        files[0].name.startsWith('TestA')
    }

    void 'Can generate a traits file'() {
        setup:
        final gv = '8.2'
        final bf = 'build.gradle'

        when:
        GenerationUtils.generateTraitsFile(
            srcOutputDir,
            loadTraitsTemplateFile(),
            fsOperations,
            testDefinition(gv, bf).packageName
        )

        final files = new File(srcOutputDir, 'test/foo').listFiles()

        then:
        files.size() == 1
        files[0].name == 'GradleTestTraits.groovy'
    }

    @Unroll
    void 'Can generate a project directory with #buildFile'() {
        setup:
        setupProjectSource()
        final gv = '8.3'
        final bf = buildFile
        final ftd = testDefinition(gv, bf)

        when:
        GenerationUtils.generateProject(srcOutputDir, ftd, false, fsOperations)
        final linkFile = new File(srcOutputDir, 'build.gradle' + (buildFile.endsWith('.kts') ? '.kts' : ''))
        final srcDir = new File(srcOutputDir, 'src')

        then:
        linkFile.exists()
        Files.readSymbolicLink(linkFile.toPath()).toFile().name == buildFile

        and:
        Files.readSymbolicLink(srcDir.toPath()).toFile().name == 'src'

        where:
        buildFile << [
            'build.gradle',
            'build.gradle.kts',
            'staged.foo.build.gradle',
            'staged.foo.build.gradle.kts'
        ]
    }

    void 'Can copy rather than symlink when requested'() {
        setup:
        setupProjectSource()
        final gv = '8.3'
        final bf = 'build.gradle'
        final ftd = testDefinition(gv, bf)

        when:
        GenerationUtils.generateProject(srcOutputDir, ftd, true, fsOperations)
        final srcDir = new File(srcOutputDir, 'src')

        then:
        srcDir.exists()
        srcDir.directory
        !Files.isSymbolicLink(srcDir.toPath())
    }

    File loadTemplateFile() {
        final template = new File(templateDir, 'GradleTestTemplateV4.groovy.template')
        fsOperations.copyResourceToFile('org/ysb33r/gradletest/GradleTestTemplateV4.groovy.template', template)
        template
    }

    File loadTraitsTemplateFile() {
        final template = new File(templateDir, 'GradleTestTraits.groovy')
        fsOperations.copyResourceToFile('org/ysb33r/gradletest/GradleTestTraits.groovy', template)
        template
    }

    FullTestDefinition testDefinition(
        String gradleVersion,
        String buildFile
    ) {
        final btd = new BasicTestDefinition(
            testDir: projectSourceDir,
            gradleVersion: gradleVersion,
            gradleBuildFile: buildFile
        )
        new FullTestDefinition(btd).tap {
            targetDir = srcOutputDir
            projectDir = new File(owner.targetDir, 'project')
            testKitDir = new File(owner.targetDir, '.testkit')
            packageName = 'test.foo'
            deprecationWarnings = ['foofoo warning']
            debug = true
            cleanCache = false
            manifestFile = Optional.empty()
            gradleDistributionUri = Optional.empty()
            defaultTask = 'runGradleTest'
            arguments = ['-s']
            configurationCache = 'warn'
            willFail = false
        }
    }

    File setupProjectSource() {
        [
            'build.gradle',
            'build.gradle.kts',
            'staged.foo.build.gradle',
            'staged.foo.build.gradle.kts',
            'settings.gradle'
        ].each {
            new File(projectSourceDir, it).text = ''
        }
        ['src'].each {
            new File(projectSourceDir, it).mkdirs()
            new File(projectSourceDir, "${it}/test.txt").text = ''
        }
    }
}
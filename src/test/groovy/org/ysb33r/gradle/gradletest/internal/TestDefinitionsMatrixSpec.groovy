/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import org.ysb33r.gradle.gradletest.BasicTestDefinition
import spock.lang.Specification
import spock.lang.Unroll

class TestDefinitionsMatrixSpec extends Specification {

    @Unroll
    void 'Generate correct class name: #input'() {
        setup:
        final gv = '8.11-rc-3'
        final btd = new BasicTestDefinition(
            testDir: new File(input),
            gradleVersion: gv,
            gradleBuildFile: 'build.gradle'

        )
        final output = GenerationUtils.classNameFrom(btd)

        expect:
        output == expected

        where:
        input     | expected
        '3me'     | '$3me__8_11_rc_3Spec'
        'me'      | 'Me__8_11_rc_3Spec'
        '%HELLO'  | '_HELLO__8_11_rc_3Spec'
        'H&*#LLO' | 'H_LLO__8_11_rc_3Spec'
    }

    void 'Can create variant class name: #input'() {
        setup:
        final gv = '8.3'
        final btd = new BasicTestDefinition(
            testDir: new File('hello'),
            gradleVersion: gv,
            gradleBuildFile: input

        )
        final output = GenerationUtils.classNameFrom(btd)

        expect:
        output == expected

        where:
        input                         | expected
        'build.gradle'                | 'Hello__8_3Spec'
        'build.gradle.kts'            | 'Hello__8_3$ktsSpec'
        'staged.foo.build.gradle'     | 'Hello__8_3$$fooSpec'
        'staged.foo.build.gradle.kts' | 'Hello__8_3$kts$$fooSpec'
    }
}
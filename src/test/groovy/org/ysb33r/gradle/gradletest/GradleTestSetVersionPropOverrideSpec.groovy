/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.ysb33r.gradle.gradletest.testfixtures.UnitTestSpecification
import spock.util.environment.RestoreSystemProperties

class GradleTestSetVersionPropOverrideSpec extends UnitTestSpecification {

    @RestoreSystemProperties
    void 'System property overrides configured versions'() {
        setup:
        System.setProperty('mineGradleTest.versions', '8.0.2,8.11.1')
        System.setProperty('gradleTest.versions', '8.0.3,8.9')
        project.pluginManager.apply(GradleTestBasePlugin)
        project.allprojects {
            gradleTestSets {
                testSets {
                    mine {
                        versions '7.3', '8.2', '8.9'
                        replaceDeprecationMessageChecksForVersion('7.3', [])
                        replaceDeprecationMessageChecksForVersion('8.11.1', [])
                    }
                    main {
                        versions '7.3'
                    }
                }
            }
        }

        final setUnderTest1 = project.extensions.getByType(GradleTestSetExtension).testSets.getByName('mine')
        final setUnderTest2 = project.extensions.getByType(GradleTestSetExtension).testSets.getByName('main')

        when:
        final versions1 = setUnderTest1.versions.get()
        final versions2 = setUnderTest2.versions.get()

        then:
        versions1.containsAll(['8.0.2', '8.11.1'])
        !versions1.contains('7.3')
        versions2.containsAll(['8.0.3', '8.9'])
        !versions2.contains('7.3')

        when:
        final msgs = setUnderTest1.deprecationMessagesAreFailures.get().versionMap

        then:
        msgs['8.11.1'].empty
        !msgs['8.0.2'].empty
        !msgs.containsKey('7.3')
        !msgs.containsKey('8.2')
    }
}
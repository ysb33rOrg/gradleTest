/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.testing.base.TestingExtension
import org.ysb33r.gradle.gradletest.internal.GradleTestSetFactory
import org.ysb33r.gradle.gradletest.internal.TestSet
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

/**
 * Extension to configure GradleTest test sets.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
class GradleTestSetExtension {
    public static final String NAME = 'gradleTestSets'
    public static final String PACKAGE_PREFIX = 'test.ysb33r.gradle.compatibility'

    final NamedDomainObjectContainer<GradleTestSet> testSets

    GradleTestSetExtension(Project project) {
        final testing = project.extensions.getByType(TestingExtension)
        this.testSetFactory = new GradleTestSetFactory(project, this)
        this.testSets = project.objects.domainObjectContainer(GradleTestSet, testSetFactory)
        this.packagePrefix = project.objects.property(String)
        this.packagePrefix.set(PACKAGE_PREFIX)
        this.testSets.whenObjectAdded { GradleTestSet testSet ->
            TestSet.addTestSetModel(testing, testSet, project)
        }
    }

    /**
     * The package name prefix that are used for compatibility tests.
     *
     * @return
     */
    Provider<String> getTestPackage() {
        this.packagePrefix
    }

    /**
     * Override the package name prefix for compatibility tests.
     *
     * @param prefix Prefix. Anything that can be converted to a string
     */
    void setTestPackage(Object prefix) {
        ccso.stringTools().updateStringProperty(this.packagePrefix, prefix)
    }

    private final Property<String> packagePrefix
    private final GradleTestSetFactory testSetFactory
    private final ConfigCacheSafeOperations ccso
}

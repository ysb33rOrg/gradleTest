/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.MapConstructor
import groovy.transform.ToString

/**
 * Describes a basic test definition.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
@MapConstructor
@EqualsAndHashCode
@ToString(includeNames = true, includePackage = false)
class BasicTestDefinition implements Serializable {
    /**
     * Directory where a specific GradleTest project is located i.e. {@code src/gradleTest/myTest}.
     */
    File testDir

    /**
     * The Gradle build file to use for the test.
     */
    String gradleBuildFile

    /**
     * The Gradle version to use for the test.
     */
    String gradleVersion
}

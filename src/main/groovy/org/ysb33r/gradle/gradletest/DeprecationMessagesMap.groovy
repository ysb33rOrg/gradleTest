/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic

/**
 * A simple class that helps to work around some limitations in Gradle property ecosystem
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
class DeprecationMessagesMap implements Serializable {
    final Map<String,List<String>> versionMap = new TreeMap<>()
}

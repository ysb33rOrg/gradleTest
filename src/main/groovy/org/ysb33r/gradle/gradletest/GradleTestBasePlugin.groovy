/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.grolifant5.api.core.plugins.GrolifantServicePlugin

/**
 * A base plugin for GradleTest. It provides only the ability to add new test sets,
 * but does not create any.
 *
 */
@CompileStatic
class GradleTestBasePlugin implements Plugin<Project> {

    public static final String SUPPORT_URL = 'https://gitlab.com/ysb33rOrg/gradleTest/-/issues'

    @Override
    void apply(Project project) {
        project.pluginManager.tap {
            apply(GrolifantServicePlugin)
            apply('groovy')
            apply('jvm-test-suite')
        }

        project.extensions.create(
            GradleTestSetExtension.NAME,
            GradleTestSetExtension,
            project
        )
    }
}

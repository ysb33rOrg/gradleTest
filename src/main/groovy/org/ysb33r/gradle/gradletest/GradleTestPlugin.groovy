/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.plugin.devel.tasks.PluginUnderTestMetadata
import org.ysb33r.grolifant5.api.core.ProjectOperations

/** Checks the current Gradle version and decides which real plugin to apply.
 *
 */
@CompileStatic
class GradleTestPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.pluginManager.tap {
            apply(GradleTestBasePlugin)

            withPlugin('java-gradle-plugin') {
                project.extensions.getByType(GradleTestSetExtension).testSets.configureEach { testSet ->
                    final metadata = project.tasks.named('pluginUnderTestMetadata', PluginUnderTestMetadata)
                        .flatMap { it.outputDirectory }
                    project.tasks.named(testSet.testTaskName, Test).configure { t ->
                        t.classpath = t.classpath + project.files(metadata)
                    }
                }
            }
        }

        ProjectOperations.find(project).tasks.whenNamed('check') {
            it.dependsOn(project.tasks.withType(Test).matching { Test t ->
                t.name == 'gradleTest' || t.name.endsWith('GradleTest')
            })
        }

        project.extensions.getByType(GradleTestSetExtension).testSets.create('main')

        if (System.getProperty('gradleTest.include') || System.getProperty('gradleTest.exclude')) {
            project.logger.warn(
                'gradleTest.(ex|in)clude system properties are no longer used on Gradle 4.4+. ' +
                    'Use --tests command-line parameter instead'
            )
        }
    }
}

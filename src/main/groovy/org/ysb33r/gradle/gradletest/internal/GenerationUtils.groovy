/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.file.CopySpec
import org.ysb33r.gradle.gradletest.BasicTestDefinition
import org.ysb33r.gradle.gradletest.FullTestDefinition
import org.ysb33r.gradle.gradletest.TestKitLocations
import org.ysb33r.gradle.gradletest.errors.BadBuildScriptName
import org.ysb33r.grolifant5.api.core.FileSystemOperations

import java.nio.file.Files
import java.util.regex.Matcher
import java.util.regex.Pattern

import static GradleScriptLanguage.GROOVY
import static GradleScriptLanguage.KOTLIN

/**
 * Helps with generation of source and data files.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
@Slf4j
class GenerationUtils {

    /**
     * Subdirectory below {@code build/gradleTest} (or similar) where a shared testkit will be placed.
     */
    public static final String TESTKIT_PROJECT_LEVEL_SUBDIR = 'testkitdata'

    /**
     * Generates a Spock test source file.
     *
     * @param targetDir Top-level directory to generate into.
     * @param templateFile Template file to use
     * @param fsOperations Grolifant {@link FileSystemOperations} instance
     * @param ftd Test definition
     *
     * @return The generates test specification file.
     */
    static File generateTest(
        File targetDir,
        File templateFile,
        FileSystemOperations fsOperations,
        FullTestDefinition ftd
    ) {
        final language = ftd.gradleBuildFile.endsWith(DOTKTS) ? KOTLIN : GROOVY
        final String testClassName = classNameFrom(ftd)
        final String testClassFileName = "${testClassName}.groovy"
        final String testPackagePath = ftd.packageName.replaceAll(LITERAL_DOT, SLASH)
        final packageDir = new File(targetDir, testPackagePath)
        final title = ftd.testDir.name +
            " ${ftd.gradleVersion.replaceAll(~/\./, UNDERSCORE)}" +
            " ${ftd.gradleBuildFile.replaceAll(~/\./, UNDERSCORE)}"

        final tokens = [
            ARGUMENTS          : quoteAndJoin(ftd.arguments),
            CHECK_WARNINGS     : tripleQuoteAndJoin(ftd.deprecationWarnings),
            CLEAN_CACHE        : ftd.cleanCache,
            CONFIGURATION_CACHE: ftd.configurationCache ?: EMPTY_STRING,
            DEFAULTTASK        : ftd.defaultTask,
            DISTRIBUTION_URI   : ftd.gradleDistributionUri.orElse(EMPTY_STRING),
            FAILMODE           : ftd.willFail,
            GRADLE_VERSION     : ftd.gradleVersion,
            LANGUAGE           : language.id,
            MANIFEST           : ftd.manifestFile ? pathAsUriStr(ftd.manifestFile.get()) : '',
            ROOTDIR            : pathAsUriStr(ftd.projectDir),
            TESTKITDIR         : pathAsUriStr(ftd.testKitDir),
            TESTNAME           : testClassName,
            TESTPACKAGE        : ftd.packageName,
            TITLE              : title,
            WITH_DEBUG         : ftd.debug
        ]

        fsOperations.copy { CopySpec cs ->
            cs.tap {
                from templateFile
                into packageDir
                rename { testClassFileName }
                expand(tokens)
            }
        }

        new File(packageDir, testClassFileName)
    }

    /**
     * Generates a traits file that is used by all tests.
     *
     * @param targetDir Top-level directory to generate into.
     * @param templateFile Template file to use
     * @param fsOperations Grolifant {@link FileSystemOperations} instance
     * @param packageName Package name
     *
     * @return Generate file
     */
    static File generateTraitsFile(
        File targetDir,
        File templateFile,
        FileSystemOperations fsOperations,
        String packageName
    ) {
        final String testPackagePath = getTestPackagePath(packageName)
        final finalTargetDir = new File(targetDir, testPackagePath)
        fsOperations.copy { CopySpec cs ->
            cs.with {
                from templateFile
                into finalTargetDir
                filter ReplaceTokens,
                    beginToken: FLAT_SCORE, endToken: FLAT_SCORE,
                    tokens: [
                        TESTPACKAGE: packageName
                    ]
            }
        }

        new File(finalTargetDir, templateFile.name)
    }

    /**
     * Remove old generated files.
     *
     * @param targetDir Targe directory
     * @param packageName Package name
     * @param generatedFiles List of known generated files.
     */
    static void removeOldGeneratedFiles(
        File targetDir,
        String packageName,
        final List<File> generatedFiles
    ) {
        final String testPackagePath = getTestPackagePath(packageName)
        final finalTargetDir = new File(targetDir, testPackagePath)
        final excludeFilenames = generatedFiles*.name
        final oldFiles = finalTargetDir.listFiles({ File it ->
            !(it.name in excludeFilenames)
        } as FileFilter)
        oldFiles.each { it.delete() }
    }

    /**
     * Converts a package name to a package path.
     *
     * @param packageName Package name
     * @return The package subpath.
     */
    static String getTestPackagePath(String packageName) {
        packageName.replaceAll(LITERAL_DOT, SLASH)
    }

    /**
     * Generates a project structure.
     *
     * <p>
     *     Mostly creates a set of symlinks.
     *     If not settings file exist in the source, one will be generated into the project directory.
     * </p>
     *
     * @param targetDir Directory to generate into
     * @param ftd Test definition.
     * @param makeCopies Copy files rather than symlink them.
     * @param fsOperation An instance of {@link FileSystemOperations} that can be used to manipulate files.
     */
    static void generateProject(
        File targetDir,
        FullTestDefinition ftd,
        boolean makeCopies,
        FileSystemOperations fsOperations
    ) {
        targetDir.deleteDir()
        targetDir.mkdirs()
        ftd.testDir.listFiles().each {
            if (it.name =~ /build\.gradle(\.kts)?$/) {
                if (it.name == ftd.gradleBuildFile) {
                    if (it.name.startsWith('staged')) {
                        final adjustedFile = new File(
                            targetDir,
                            it.name.endsWith(DOTKTS) ? 'build.gradle.kts' : 'build.gradle'
                        )
                        createSymbolicLink(adjustedFile, it)
                    } else {
                        createSymbolicLink(new File(targetDir, it.name), it)
                    }
                }
            } else {
                if (makeCopies && it.directory) {
                    fsOperations.sync { cs ->
                        cs.from(it)
                        cs.into(new File(targetDir, it.name))
                        cs.include '**'
                    }
                } else {
                    createSymbolicLink(new File(targetDir, it.name), it)
                }
            }
        }
        final settingsFile = new File(targetDir, GROOVY.settingsFilePattern)
        if (!settingsFile.exists() && !new File(KOTLIN.settingsFilePattern).exists()) {
            settingsFile.text = ''
        }
    }

    /**
     * Sanitized the name of a directory into seomthing that can be used to start a Groovy class name.
     *
     * @param name Name of a directory
     * @return Sanitized class name initial part.
     */
    static String classNameComponentBase(String name) {
        final initBaseName = name.capitalize()
            .replaceAll(~/\W/, UNDERSCORE)
            .replaceAll(~/_{2,}/, UNDERSCORE)

        (initBaseName =~ /^\d.*/) ? '$' + initBaseName : initBaseName
    }

    /**
     * Sanitizes a Gradle version into something that can be used in a class name.
     *
     * @param gradleVersion Gradle version
     * @return Class name part.
     */
    static String classNameComponentVersion(String gradleVersion) {
        gradleVersion?.replaceAll(~/\.|-/, UNDERSCORE) ?: EMPTY_STRING
    }

    /**
     * Calcaultes a postfix for a class name based upon the build file type.
     *
     * @param buildFile Build file name.
     * @return Sanitized appendage.
     */
    static String classNameComponentBuildFile(String buildFile) {
        switch (buildFile) {
            case GROOVY.buildFilePattern:
                return EMPTY_STRING
            case KOTLIN.buildFilePattern:
                return '$kts'
            default:
                final val = buildFile =~ EXTRACTOR
                if (!val.matches()) {
                    throw new BadBuildScriptName("'${buildFile}' is invalid")
                }
                final name = extract(val)
                final insert = buildFile.endsWith(DOTKTS) ? '$kts$$' : '$$'
                return "${insert}${name}"
        }
    }

    /**
     * Builds a class name from a set of sanitized components.
     *
     * @param sanitizedBase Sanitized base name
     * @param sanitizedVersion Sanitized Gradle version
     * @param sanitizedBuildFile Sanitized build file type
     * @return class name suitable for a Groovy source file or potentially for a tst filter.
     */
    static String classNameFromComponents(
        String sanitizedBase,
        String sanitizedVersion,
        String sanitizedBuildFile
    ) {
        "${sanitizedBase}__${sanitizedVersion}${sanitizedBuildFile}Spec"
    }

    /**
     * Builds a class name from a test definition.
     *
     * @param btd TEst definition
     * @return class name suitable for a Groovy source file.
     */
    static String classNameFrom(BasicTestDefinition btd) {
        final baseName = classNameComponentBase(btd.testDir.name)
        final versionInName = classNameComponentVersion(btd.gradleVersion)
        final bf = classNameComponentBuildFile(btd.gradleBuildFile)

        classNameFromComponents(baseName, versionInName, bf)
    }

    /** Ensures that files are represented as URIs.
     * (This helps with compatibility across operating systems).
     *
     * @param path Path to output
     */
    static String pathAsUriStr(final File path) {
        path.absoluteFile.toURI().toString()
    }

    /**
     * Create a string suitable for sending to the Spock template.
     *
     * @param testKitLocation
     * @return A string representing a boolean value or a null value
     */
    static String testKitLocationString(TestKitLocations testKitLocation) {
        switch (testKitLocation) {
            case TestKitLocations.PER_TEST:
                return 'true'
            case TestKitLocations.PER_GROUP:
                return 'false'
            default:
                'null'
        }
    }

    static String quoteAndJoin(Iterable<String> c) {
        c.collect { "'${it}'" }.join(COMMA)
    }

    static String tripleQuoteAndJoin(Iterable<String> c) {
        c.collect { "'''${it.replaceAll(BACKSLASH, DOUBLE_BACKSLASH)}'''" }.join(COMMA)
    }

    @CompileDynamic
    private static String extract(final Matcher filename) {
        filename[0][1]
    }

    private static void createSymbolicLink(File link, File target) {
        final linkToBeCreated = link.toPath()
        Files.deleteIfExists(linkToBeCreated)
        Files.createSymbolicLink(
            linkToBeCreated,
            target.toPath()
        )
    }

    private static final String LITERAL_DOT = '\\.'
    private static final String UNDERSCORE = '_'
    private static final String FLAT_SCORE = '____'
    private static final String COMMA = ','
    private static final String SLASH = '/'
    private static final String DOTKTS = '.kts'
    private static final String EMPTY_STRING = ''
    private static final String BACKSLASH = '\\\\'
    private static final String DOUBLE_BACKSLASH = '\\\\\\\\'
    private static final Pattern EXTRACTOR = ~/^staged\.(.+?)\.build\.gradle(\.kts)?$/
}

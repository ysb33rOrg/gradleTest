/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.testing.Test
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.ysb33r.gradle.gradletest.GradleTestSet
import org.ysb33r.gradle.gradletest.TestGenerator

import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_8_0

/** Configures GradleTest tasks for Jacoco.
 *
 * @since 2.0
 */
@CompileStatic
class Jacoco {
    /**
     * Adds the given test task to Jacoco reporting.
     *
     * @param testTask Test task provider
     * @param project Associated project
     *
     * @since 4.0
     */
    static void configureWhenApplied(TaskProvider<Test> testTask, Project project) {
        project.pluginManager.withPlugin(JACOCO_ID) {
            if (PRE_8_0) {
                final jtr = project.tasks.named('jacocoTestReport', JacocoReport)
                jtr.configure { jr -> jr.executionData(testTask) }
            } else {
                testTask.configure { it.extensions.getByType(JacocoTaskExtension).enabled = true }
            }
        }
    }

    /**
     * Configures Gradle test sets when Jacoco is applied.
     *
     * <p>
     *     Debug is turned on for all Gradle tests.
     *     Debug warnings are turned off.
     * </p>
     *
     * @param testSet Gradle test set.
     * @param project Associated project.
     */
    static void configureGradleTestSets(GradleTestSet testSet, Project project) {
        project.pluginManager.withPlugin(JACOCO_ID) {
            project.tasks.named(testSet.testSourceGeneratorTaskName, TestGenerator) { t ->
                t.debugWarnings = project.provider { -> false }
            }

            testSet.debug = true
        }
    }

    private static final String JACOCO_ID = 'jacoco'
}

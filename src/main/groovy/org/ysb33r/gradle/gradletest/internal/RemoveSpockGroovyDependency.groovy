/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import org.gradle.api.artifacts.CacheableRule
import org.gradle.api.artifacts.ComponentMetadataContext
import org.gradle.api.artifacts.ComponentMetadataRule
import org.gradle.api.artifacts.DirectDependenciesMetadata

/**
 * Component corrector to keep Spock from adding Groovy as transitive dependency.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CacheableRule
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class RemoveSpockGroovyDependency implements ComponentMetadataRule {
    void execute(ComponentMetadataContext context) {
        context.details.allVariants { vm ->
            vm.withDependencies { DirectDependenciesMetadata dtd ->
                dtd.removeAll { it.group in ['org.codehaus.groovy'] }
            }
        }
    }
}
/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.JavaVersion
import org.gradle.api.provider.Provider
import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.jvm.toolchain.JavaLauncher
import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.BasicTestDefinition

import static GradleVersions.MINIMUM_SUPPORTED
import static org.ysb33r.gradle.gradletest.GradleTestBasePlugin.SUPPORT_URL
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.CURRENT
import static org.ysb33r.gradle.gradletest.internal.JdkSupport.minimumGradleForCurrentJavaVersion

/**
 * Calculates the matrix of tests.
 *
 * @since 4.0
 */
@CompileStatic
@Slf4j
class TestDefinitionsMatrix {

    /**
     * Builds a test matrix after analysing the data.
     *
     * @param versions Gradle versions to be used for testing.
     * @param javaLanguageVersion Java language version that will be used to run the tests.
     * @param sourceDir Source directory for the test definitions.
     * @return Matrix of tests.
     */
    static Provider<List<BasicTestDefinition>> buildMatrixFromLanguageVersion(
        Provider<Set<String>> versions,
        Provider<JavaLanguageVersion> javaLanguageVersion,
        Provider<File> sourceDir
    ) {
        final filteredVersions = versionsToUse(versions, javaLanguageVersion)
        final projects = scanGradleProjects(dirScanner(sourceDir))

        filteredVersions.zip(projects) { vers, prj ->
            vers.collectMany { ver ->
                prj.collect {
                    new BasicTestDefinition(
                        testDir: it.testDir,
                        gradleBuildFile: it.gradleBuildFile,
                        gradleVersion: ver
                    )
                }
            }
        }
    }

    /**
     * Builds a test matrix after analysing the data.
     *
     * @param versions Gradle versions to be used for testing.
     * @param javaLauncher Java launcher that will be used to run the tests.
     * @param sourceDir Source directory for the test definitions.
     * @return Matrix of tests.
     */
    static Provider<List<BasicTestDefinition>> buildMatrixFromJavaLauncher(
        Provider<Set<String>> versions,
        Provider<JavaLauncher> javaLauncher,
        Provider<File> sourceDir
    ) {
        buildMatrixFromLanguageVersion(
            versions,
            javaLauncher.map { it.metadata.languageVersion },
            sourceDir
        )
    }

    /**
     * Scans the source directory for Gradle project directories
     * @param sourceDir GradleTest source directory
     * @return List of project directories. Never null. Can be empty.
     */
    static Provider<List<File>> dirScanner(Provider<File> sourceDir) {
        sourceDir.map { f ->
            final dirs = f.listFiles({ File it -> it.directory } as FileFilter)?.toList() ?: []
            dirs.findAll { dir ->
                dir.listFiles({ File d, String n ->
                    final name = n.toLowerCase(Locale.US)
                    name.endsWith('build.gradle') || name.endsWith('build.gradle.kts')
                } as FilenameFilter)
            }
        }
    }

    static Provider<List<BasicTestDefinition>> scanGradleProjects(
        Provider<List<File>> projectDirs
    ) {
        projectDirs.map { dirs ->
            dirs.collectMany { dir ->
                final List<BasicTestDefinition> tds = []
                dir.listFiles().each { File f ->
                    if (f.name == BUILD_GROOVY || f.name == BUILD_KOTLIN ||
                        f.name =~ /staged\..+?\.build.gradle(\.kts)?$/
                    ) {
                        tds.add(new BasicTestDefinition(testDir: dir, gradleBuildFile: f.name))
                    }
                }
                tds
            }
        }
    }

    /**
     * Creates a filtered collection of versions.
     *
     * @param versions Initial versions.
     * @param javaLanguageVersion Java Launcher that will run the tests.
     * @return Provider to filtered version.
     */
    static Provider<Set<String>> versionsToUse(
        Provider<Set<String>> versions,
        Provider<JavaLanguageVersion> javaLanguageVersion
    ) {
        final supportedVersions = versions.map { vers ->
            final result = [] as SortedSet<String>
            result.addAll(vers)
            final Collection<String> removeTheseBecauseGradle = vers.findAll { String v ->
                GradleVersion.version(v) < MINIMUM_SUPPORTED
            }
            if (!removeTheseBecauseGradle.empty) {
                log.warn(
                    "Gradle version(s) '${removeTheseBecauseGradle.join(COMMA)}' is/are not supported by this " +
                        'GradleTest-Gradle combination and will be removed from the test set. ' +
                        "(requires >=${MINIMUM_SUPPORTED.version})."
                )
                result.removeAll(removeTheseBecauseGradle)
            }
            result
        }

        final minGradleForJdk = javaLanguageVersion.map {
            JdkSupport.getMinimumGradleForJavaVersion(it.asInt())
        }.orElse(minimumGradleForCurrentJavaVersion())

        final currentJdk = javaLanguageVersion.map { it.toString() }
            .orElse(JavaVersion.current().majorVersion)

        minGradleForJdk.zip(supportedVersions) { gv, vers ->
            final result = [] as SortedSet
            result.addAll(vers)
            final Collection<String> removeTheseBecauseJdk = vers.findAll { String v ->
                GradleVersion.version(v) < gv
            }
            if (!removeTheseBecauseJdk.empty) {
                log.warn(
                    "Gradle versions '${removeTheseBecauseJdk.join(COMMA)}' are not supported by " +
                        "JDK${currentJdk.get()} and will be removed from the test set"
                )
                result.removeAll(removeTheseBecauseJdk)
            }
            result as Set<String>
        }.orElse(supportedVersions.map {
            log.warn(
                "Current JDK${currentJdk.get()} might not be fully supported by GradleTest. " +
                    'Removing all Gradle versions from testing except current. YMMV. ' +
                    "Please raise an issue at ${SUPPORT_URL} for GradleTest support."
            )
            [CURRENT.version].toSet()
        })
    }

    private static final String COMMA = ','
    private static final String BUILD_GROOVY = GradleScriptLanguage.GROOVY.buildFilePattern
    private static final String BUILD_KOTLIN = GradleScriptLanguage.KOTLIN.buildFilePattern
}

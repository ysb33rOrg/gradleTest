/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.gradle.util.GradleVersion

/** Fixed constants and methods for dealing with specific Gradle versions.
 *
 * @since 2.0
 */
@CompileStatic
class GradleVersions {
    public static final GradleVersion CURRENT = GradleVersion.current()
    public static final GradleVersion GRADLE_4_0 = GradleVersion.version('4.0')

    /**
     * Minimum version supported by GradleTest.
     *
     * @since 3.0
     */
    public static final GradleVersion MINIMUM_SUPPORTED = GRADLE_4_0

    /**
     * Minimum version that supports Kotlin DSL.
     *
     * @since 4.0
     */
    public final static GradleVersion MINIMUM_SUPPORTED_KOTLIN_DSL =
        GradleVersion.version('4.10')

    /**
     * Minimum version for Kotlin DSL where debugging can be used.
     *
     * @since 4.0
     */
    public final static GradleVersion MINIMUM_SUPPORTED_KOTLIN_DEBUG =
        GradleVersion.version( '6.0')

    /**
     * Minimum version for which debug is turned on in presence of configuration cache.
     *
     * @since 4.0
     */
    public final static GradleVersion MINIMUM_SUPPORTED_CONFIGURATION_CACHE_DEBUG =
        GradleVersion.version('100.0')

    /**
     * Minimum version for which GradleTest will allow configuration cache testing.
     *
     * @since 4.0
     */
    public final static GradleVersion MINIMUM_SUPPORTED_FOR_CONFIGURATION_CACHE =
        GradleVersion.version('7.0')
}

/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.ysb33r.gradle.gradletest.ConfigurationCacheMode

/**
 * An internal class to hold information about each Gradle that is in a test set.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
@ToString(includeNames = true, includePackage = false)
@EqualsAndHashCode
class VersionExecutionProperties implements Serializable {
    /**
     * The configuration cache mode for this version.
     */
    ConfigurationCacheMode ccMode = ConfigurationCacheMode.NONE

    /**
     * The deprecation warnings for this version that needs to be checked.
     */
    List<String> deprecationWarningsMap = Collections.EMPTY_LIST
}

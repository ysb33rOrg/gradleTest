/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.plugins.jvm.JvmTestSuite
import org.gradle.api.plugins.jvm.JvmTestSuiteTarget
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.api.tasks.testing.Test
import org.gradle.language.base.plugins.LifecycleBasePlugin
import org.gradle.testing.base.TestingExtension
import org.ysb33r.gradle.gradletest.ClasspathManifest
import org.ysb33r.gradle.gradletest.GradleTestSet
import org.ysb33r.gradle.gradletest.TestGenerator
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.ProviderTools

import javax.inject.Inject

import static org.ysb33r.gradle.gradletest.internal.GenerationUtils.classNameComponentVersion
import static org.ysb33r.gradle.gradletest.internal.GenerationUtils.classNameFromComponents
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_4
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_7_6
import static org.ysb33r.grolifant5.api.core.LegacyLevel.PRE_9_0

/**
 * Internal utility functions to add a new GradleTest test set.
 *
 * These methods should only be called during configuration phase.
 *
 * @since 1.0
 */
@CompileStatic
class TestSet {

    public final static String SPOCK_V2_VERSION = '2.3'

    /**
     * A class that is used to register a manifest task if it does not exist.
     *
     * I tis primarily used so that the test set does not need to keep a copy of the {@code Project} instance.
     */
    static class ManifestRegistrar {
        @Inject
        ManifestRegistrar(Project project, GradleTestSet testSet) {
            this.project = project
            this.testSet = testSet
        }

        void maybeRegister() {
            if (!project.tasks.names.contains(testSet.customTestManifestTaskName)) {
                registerManifestTask(testSet, project)
            }
        }

        private final GradleTestSet testSet
        private final Project project
    }
    /** Adds a test set with default dependencies
     *
     * @param project Project to add tests to
     * @param testSetName Name of test set
     *
     * @since 4.0
     */
    static void addTestSetModel(
        TestingExtension testing,
        GradleTestSet testSet,
        Project project
    ) {
        registerTestTask(testing, testSet, project)
        registerTestGenerator(testSet, project)
        disableLicenseSourceChecks(testSet, project)
        disableCodenarcSourceChecks(testSet, project)
        configureSourceSet(testing, testSet, project)
        handleGroovyConflict(project)
        Jacoco.configureGradleTestSets(testSet, project)
    }

    /**
     * Registers the test task for the test set and configures the source set that provides the test classes.
     *
     * @param testing The testing extension for the project.
     * @param testSet Associated test set
     * @param project Associated project
     */
    static void registerTestTask(
        TestingExtension testing,
        GradleTestSet testSet,
        Project project
    ) {

        testing.suites.create(testSet.testTaskName, JvmTestSuite) { jts ->
            jts.useSpock(spockMode)
            addGradleTestKit(jts, project)
            setTestType(testSet.name, jts)

            jts.targets.all { JvmTestSuiteTarget jtst ->
                jtst.testTask.configure {
                    final filterTestToVersions = testFiltersFromOutsideVersions(
                        it.name,
                        ProjectOperations.find(project).providerTools
                    ).get()
                    it.group = Names.TASK_GROUP
                    it.description = 'Runs Gradle compatibility tests'
                    it.mustRunAfter 'test', testSet.testSourceGeneratorTaskName
                    it.systemProperties 'org.gradle.console': 'plain'
                    it.inputs.dir(testSet.sourceDirectory).optional()

                    it.filter { tf ->
                        tf.includePatterns.addAll(filterTestToVersions)
                    }
                }
                Jacoco.configureWhenApplied(jtst.testTask, project)
            }
        }
    }

    /**
     * Registers the task that creates the test sources.
     *
     * @param testSet Associated test set
     * @param project Associated project.
     */
    static void registerTestGenerator(
        GradleTestSet testSet,
        Project project
    ) {
        final genTaskName = testSet.testSourceGeneratorTaskName
        final testTaskName = testSet.testTaskName
        final javaLauncher = project.tasks.named(testTaskName, Test).flatMap {
            it.javaLauncher
        }

        project.tasks.named(testTaskName).configure {
            it.dependsOn(genTaskName)
        }

        project.tasks.register(genTaskName, TestGenerator) { t ->
            t.tap {
                group = LifecycleBasePlugin.BUILD_GROUP
                description = 'Creates text fixtures for compatibility testing'

                testRootDirectory = testSet.sourceDirectory
                sourceOutputDir = testSet.generatedSourceDir
                projectOutputDir = fsOperations().buildDirDescendant(testSet.testTaskName)
                testKitStrategy = testSet.testKitStrategy
                packageName = testSet.package
                deprecationMessagesMap = testSet.deprecationMessagesAreFailures
                configurationCacheMap = testSet.configurationCacheModeMapping
                taskToRun = testSet.defaultTaskToRun
                cleanCache = testSet.cleanCache
                expectedFailures = testSet.expectedFailures
                gradleDistributionURI = testSet.gradleDistributionURI
                gradleArguments = testSet.gradleArguments
                debug = testSet.debug
                copyOverSymlink = testSet.copyOverSymlink
                testMapProvider = TestDefinitionsMatrix.buildMatrixFromJavaLauncher(
                    testSet.versions,
                    javaLauncher,
                    testSet.sourceDirectory
                )
            }
        }
    }

    /**
     * Registers the task that creates the test plugin manifest.
     *
     * @param testSet Associated test set
     * @param project Associated project
     */
    static registerManifestTask(
        GradleTestSet testSet,
        Project project
    ) {
        final configName = project.extensions.getByType(SourceSetContainer)
            .getByName(testSet.testTaskName)
            .runtimeClasspathConfigurationName
        final fc = project.configurations.getByName(configName)

        final manifest = project.tasks.register(
            testSet.customTestManifestTaskName,
            ClasspathManifest
        ) {
            it.group = Names.TASK_GROUP
            it.description = "Creates manifest file for ${testSet.testTaskName}"
            it.outputDir = it.fsOperations().buildDirDescendant(testSet.testTaskName)
            it.classpath.from(fc)
        }

        final taskTools = ProjectOperations.find(project).tasks
        final mf = manifest.flatMap { it.outputFile }
        taskTools.whenNamed(testSet.testSourceGeneratorTaskName, TestGenerator) {
            it.manifestFile = mf
        }

        taskTools.whenNamed(testSet.testTaskName, Test) {
            it.dependsOn(manifest)
        }
    }

    /**
     * Get a testset base name
     *
     * @param name Name to use as seed
     * @return Name that will be used as basename for configurations and tasks
     */
    static String baseName(final String testSetName) {
        testSetName == Names.DEFAULT_TEST_SET_NAME ? Names.DEFAULT_TASK : "${testSetName.uncapitalize()}GradleTest"
    }

    /**
     * Obtains a list of versions that were supplied outside of configuration i.e. via Gradle property,
     * system property or environment variable.
     *
     * @param baseName Name of test task.
     * @param pt An instance of {@link ProviderTools}.
     * @return Provider to a list of versions.
     */
    static Provider<List<String>> testVersionsFromOutside(
        String baseName,
        ProviderTools pt
    ) {
        pt.resolveProperty("${baseName}.versions", '')
            .map { it.empty ? [] : it.split(',')?.toList() }
    }

    /**
     * Obtains a list of test filters that can be applied based upon outside versions.
     *
     * @param baseName Name of test task.
     * @param pt An instance of {@link ProviderTools}.
     * @return Provider to a list of filters.
     */
    static Provider<List<String>> testFiltersFromOutsideVersions(
        String baseName,
        ProviderTools pt
    ) {
        testVersionsFromOutside(baseName, pt).map { versions ->
            versions.collect {
                classNameFromComponents(
                    WILDCARD * 2,
                    classNameComponentVersion(it),
                    WILDCARD
                ) + WILDCARD
            }
        }
    }

    /**
     * Gets the relative source dir where test code will be generated into.
     *
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A relative path to a root directory for source generation.
     */
    static String getRelativeSourceDir(final String testSetBaseName) {
        final String postfix = "/${LANG_NAME}"
        ".generated-src/gradleTestPlugin/${testSetBaseName}/src${postfix}"
    }

    /**
     * The name of the test generator task.
     *
     * @param testSetBaseName Name of the test task.
     * @return Name of the test generator task.
     */
    static String getGeneratorTaskName(final String testSetBaseName) {
        "${testSetBaseName}${Names.GENERATOR_TASK_POSTFIX}"
    }

    /**
     * The name of the test manifest generator task.
     *
     * @param testSetBaseName Name of the test task.
     * @return Name of the test manifest generator task.
     */
    static String getManifestTaskName(final String testSetBaseName) {
        "${testSetBaseName}${Names.MANIFEST_TASK_POSTFIX}"
    }

    /**
     * The name of the task from the License plugin that can can format a sourceset.
     *
     * <p>
     *     We identify this task so that it can be disabled.
     * </p>
     *
     * @param testSetBaseName The name of the test task
     * @return The name of the license task associated with the source set.
     */
    static String getLicenseTaskName(final String testSetBaseName) {
        "license${testSetBaseName.capitalize()}"
    }

    /**
     * The name of the task from the Codenarc plugin that can can format a sourceset.
     *
     * <p>
     *     We identify this task so that it can be disabled.
     * </p>
     *
     * @param testSetBaseName The name of the test task
     * @return The name of the codenarc task associated with the source set.
     */
    static String getCodenarcTaskName(final String testSetBaseName) {
        "codenarc${testSetBaseName.capitalize()}"
    }

    /**
     * Returns which version of Spock to use.
     *
     * @return The full version of the Spock dependency
     */
    static String getSpockMode() {
        final String gVer = GroovySystem.version
        final String spockVer = SPOCK_V2_VERSION

        "${spockVer}-groovy-${gVer.replaceAll(/\.\d+(-SNAPSHOT)?$/, '')}"
    }

    /**
     * Configures a source set relevant to a test set.
     * @param testing The testing extension.
     * @param testSet The test set itself.
     * @param project Associated project.
     */
    static void configureSourceSet(
        TestingExtension testing,
        GradleTestSet testSet,
        Project project
    ) {
        final testGenerator = project.tasks.named(testSet.testSourceGeneratorTaskName, TestGenerator)
        testing.suites.named(testSet.testTaskName, JvmTestSuite).configure {
            it.sources { ss ->
                ss.java.srcDirs = []
                ss.resources.srcDirs = []
                final groovy = groovySourceDirectorySet(ss)
                groovy.srcDirs = [testGenerator.flatMap { it.sourceOutputDir }]

                project.tasks.named(ss.getCompileTaskName(LANG_NAME), GroovyCompile).configure { t ->
                    t.doFirst {
                        t.destinationDirectory.get().asFile.deleteDir()
                    }
                    t.dependsOn(testGenerator)
                }
            }
        }
    }

    /**
     * Disables license checking if the Hierynomus License plugin is applied.
     *
     * @param testSet Associated test set.
     * @param project Associated project.
     */
    static void disableLicenseSourceChecks(GradleTestSet testSet, Project project) {
        project.pluginManager.withPlugin('com.github.hierynomus.license') {
            project.tasks.named(getLicenseTaskName(testSet.testTaskName)).configure { t ->
                t.enabled = false
            }
        }
    }

    /**
     * Disables Codenarc tasks when the Codenarc plugin is applied
     *
     * @param testSet Associated test set.
     * @param project Associated project.
     */
    static void disableCodenarcSourceChecks(GradleTestSet testSet, Project project) {
        project.pluginManager.withPlugin('codenarc') {
            project.tasks.named(getCodenarcTaskName(testSet.testTaskName)).configure { t ->
                t.enabled = false
            }
        }
    }

    @CompileDynamic
    private static SourceDirectorySet groovySourceDirectorySet(SourceSet ss) {
        ss.groovy
    }

    @CompileDynamic
    private static void setTestType(String testSetName, JvmTestSuite jts) {
        if (!PRE_7_4) {
            final postfix = testSetName == 'main' ? '' : "-${testSetName.toLowerCase(Locale.US)}"
            jts.testType.set("gradle-compatibility-tests${postfix}")
        }
    }

    // We need this method because localGroovy is not participating in conflict resolution.
    // Unfortunately it affects Spock across all configurations.
    // See https://github.com/gradle/gradle/issues/29049
    // Our issue: https://gitlab.com/ysb33rOrg/gradleTest/-/issues/152
    private static void handleGroovyConflict(Project project) {
        project.dependencies.components.withModule(
            SPOCK_MODULE,
            RemoveSpockGroovyDependency
        )
    }

    // Needs to be dynamic as JvmComponentDependencies.implementation is different between
    // 7.x & 8.x
    @CompileDynamic
    private static void addGradleTestKit(JvmTestSuite jts, Project project) {
        jts.dependencies { jcd ->
            if (PRE_7_6) {
                jcd.implementation(project.dependencies.gradleTestKit())
            } else {
                jcd.implementation.add(jcd.gradleTestKit())
            }

            // There is some issue whereby JUnit platform launcher for 1.9.0 causes an issue.
            // If we find that version, we force to 1.11.3.
            if (PRE_9_0 && !PRE_7_6) {
                jcd.runtimeOnly.add('org.junit.platform:junit-platform-launcher:1.11.3')
            }
        }
    }

    private static final String LANG_NAME = 'groovy'
    private static final String WILDCARD = '*'
    private static final String SPOCK_MODULE = 'org.spockframework:spock-core'
}

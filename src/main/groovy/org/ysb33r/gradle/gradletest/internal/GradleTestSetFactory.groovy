/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.gradle.api.NamedDomainObjectFactory
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.ysb33r.gradle.gradletest.GradleTestSet
import org.ysb33r.gradle.gradletest.GradleTestSetExtension

/**
 * A factory for {@link GradleTestSet} instances.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
class GradleTestSetFactory implements NamedDomainObjectFactory<GradleTestSet> {

    GradleTestSetFactory(Project project, GradleTestSetExtension parent) {
        this.objectFactory = project.objects
        this.parent = parent
    }

    @Override
    GradleTestSet create(String name) {
        objectFactory.newInstance(GradleTestSet, name, parent)
    }

    private final GradleTestSetExtension parent
    private final ObjectFactory objectFactory
}

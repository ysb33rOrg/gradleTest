/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.IgnoreEmptyDirectories
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Optional as DirectoryOptional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SkipWhenEmpty
import org.gradle.api.tasks.TaskAction
import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.internal.GenerationUtils
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import java.util.regex.Pattern

import static org.ysb33r.gradle.gradletest.TestKitLocations.PER_GROUP
import static org.ysb33r.gradle.gradletest.TestKitLocations.PER_TEST
import static org.ysb33r.gradle.gradletest.TestKitLocations.SHARED
import static org.ysb33r.gradle.gradletest.internal.GenerationUtils.TESTKIT_PROJECT_LEVEL_SUBDIR
import static org.ysb33r.gradle.gradletest.internal.GradleScriptLanguage.GROOVY
import static org.ysb33r.gradle.gradletest.internal.GradleScriptLanguage.KOTLIN
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.MINIMUM_SUPPORTED_CONFIGURATION_CACHE_DEBUG
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.MINIMUM_SUPPORTED_FOR_CONFIGURATION_CACHE
import static org.ysb33r.gradle.gradletest.internal.GradleVersions.MINIMUM_SUPPORTED_KOTLIN_DEBUG

/**
 * Generates test files that will be compiled against GradleTestKit and project files for execution.
 *
 * <p>
 *     Both the test task and the compile task are the dependent on this task executing correctly.
 * </p>
 *
 * @since 1.0
 */
@CompileStatic
class TestGenerator extends GrolifantDefaultTask {

    @SuppressWarnings('AbcMetric')
    TestGenerator() {
        this.testSourceDirProvider = project.objects.property(File)
        this.sourceOutputDirProvider = project.objects.property(File)
        this.projectOutputDirProvider = project.objects.property(File)
        this.testKitLocationsProvider = project.objects.property(TestKitLocations)
        this.deprecationMessagesMapProperty = project.objects.property(DeprecationMessagesMap)
        this.useDebug = project.objects.property(Boolean)
        this.copyOverSymlink = project.objects.property(Boolean)
        this.debugWarnings = project.objects.property(Boolean)
        this.cleanCache = project.objects.property(Boolean)
        this.defaultTask = project.objects.property(String)
        this.pkgName = project.objects.property(String)
        this.gradleDistributionURI = project.objects.property(URI)
        this.arguments = project.objects.listProperty(String)
        this.expectedFailures = project.objects.listProperty(Pattern)
        this.testListProvider = project.objects.listProperty(BasicTestDefinition)
        this.configurationCacheModeMapProperty = project.objects.mapProperty(String, ConfigurationCacheMode)
        this.templateDir = fsOperations().buildDirDescendant("tmp/gradleTest/templates/${name}")
        this.manifestFileProvider = project.objects.property(File)
        this.debugWarnings.set(true)

        configureInputs()
        configureConditionalExecution()
    }

    /**
     * Sets the package name.
     *
     * @param pkg Name of package.
     *
     * @since 4.0
     */
    void setPackageName(Provider<String> pkg) {
        this.pkgName.set(pkg)
    }

    /**
     * Sets the testkit sharing strategy.
     *
     * @param tkl
     */
    void setTestKitStrategy(Provider<TestKitLocations> tkl) {
        this.testKitLocationsProvider.set(tkl)
    }

    /**
     * The directory where code is generated into.
     *
     * @return Output directory.
     *
     * @since 4.0
     */
    @OutputDirectory
    Provider<File> getSourceOutputDir() {
        this.sourceOutputDirProvider
    }

    /**
     * Sets the toplevel directory where code is to be generated into.
     *
     * @param out Output directory provider.
     *
     * @since 4.0
     */
    void setSourceOutputDir(Provider<File> out) {
        this.sourceOutputDirProvider.set(out)
    }

    /**
     * The toplevel directory where the project is generated into.
     *
     * @return Output directory.
     *
     * @since 4.0
     */
    @OutputDirectory
    Provider<File> getProjectOutputDir() {
        this.projectOutputDirProvider
    }

    /**
     * Sets the toplevel directory where the project is generated into.
     *
     * @param out Output directory provider.
     *
     * @since 4.0
     */
    void setProjectOutputDir(Provider<File> out) {
        this.projectOutputDirProvider.set(out)
    }

    /**
     * The root directory where to find tests for this specific GradleTest grouping
     * The default root directory by convention is {@code src/gradleTest}. The patterns for the
     * directory is {@code src/} + {@code gradleTestSetName}.
     *
     * @return The directory as a file object resolved as per {@code project.file ( )}.
     */
    @InputDirectory
    @DirectoryOptional
    @IgnoreEmptyDirectories
    @SkipWhenEmpty
    Provider<File> getTestRootDirectory() {
        this.testSourceDirProvider
    }

    /**
     * Sets the source directory for tests.
     *
     * @param srcDir Source directory
     *
     * @since 4.0
     */
    void setTestRootDirectory(Provider<File> srcDir) {
        this.testSourceDirProvider.set(srcDir)
    }

    /**
     * Provides the map of test definitions.
     *
     * @param testMap Test definitions map.
     *
     * @since 4.0
     */
    void setTestMapProvider(Provider<List<BasicTestDefinition>> testMap) {
        this.testListProvider.set(testMap)
    }

    /**
     * Configures a map that will return the deprecation message patterns on a per Gradle-version basis.
     *
     * @param map Maps Gradle versions to deprecation message checks.
     *
     * @since 4.0
     */
    void setDeprecationMessagesMap(Provider<DeprecationMessagesMap> map) {
        this.deprecationMessagesMapProperty.set(map)
    }

    /**
     * Whether debug should be available on tests.
     *
     * @param dbg Debug mode
     *
     * @since 4.0
     */
    void setDebug(Provider<Boolean> dbg) {
        this.useDebug.set(dbg)
    }

    /**
     * Sets whether warnings about turning debug off on tests are printed.
     *
     * <p>When warnings are turned off, they are sent to the debug logger instead.</p>
     *
     * @param dbg Debug warnings mode.
     *
     * @since 4.0
     */
    void setDebugWarnings(Provider<Boolean> dbg) {
        this.debugWarnings.set(dbg)
    }

    /**
     * Whether copies rather than symlinks should be performed when generating the project structure..
     *
     * @param dbg Preference provider. {@code true} means copies will be made.
     *
     * @since 4.0
     */
    void setCopyOverSymlink(Provider<Boolean> mode) {
        this.copyOverSymlink.set(mode)
    }

    /**
     * Sets the configuration cache mapping for Gradle versions.
     *
     * <p>
     *     For older versions of Gradle, configuration cache will not be used.
     * </p>
     *
     * @param ccMap mapping
     *
     * @since 4.0
     */
    void setConfigurationCacheMap(Provider<Map<String, ConfigurationCacheMode>> ccMap) {
        this.configurationCacheModeMapProperty.set(ccMap)
    }

    /**
     * Sets the task to run inside the project.
     *
     * @param taskName Task name.
     *
     * @since 4.0
     */
    void setTaskToRun(Provider<String> taskName) {
        this.defaultTask.set(taskName)
    }

    /**
     * Sets whether the cache should be cleaned before each test run.
     *
     * @param cc Check provider.
     *
     * @since 4.0
     */
    void setCleanCache(Provider<Boolean> cc) {
        this.cleanCache.set(cc)
    }

    /**
     * Sets additional arguments that must be passed to Gradle.
     *
     * @param args Provider of arguments.
     *
     * @since 4.0
     */
    void setGradleArguments(Provider<List<String>> args) {
        this.arguments.set(args)
    }

    /**
     * Sets which test groups are expected to fail.
     *
     * @param patterns Failure patterns.
     */
    void setExpectedFailures(Provider<List<Pattern>> patterns) {
        this.expectedFailures.set(patterns)
    }

    /**
     * Set a location from where to get Gradle distributions from.
     *
     * @param uriProvider Location provider.
     */
    void setGradleDistributionURI(Provider<URI> uriProvider) {
        this.gradleDistributionURI.set(
            uriProvider.map { uri ->
                uri.path.endsWith(SLASH) ? uri : new URI(uri.toString() + SLASH)
            })
    }

    /**
     * Custom manifest.
     *
     * @param manifest Sets a provider for a manifest file.
     *
     * @since 4.0
     */
    void setManifestFile(Provider<File> manifest) {
        this.manifestFileProvider.set(manifest)
    }

    @TaskAction
    void exec() {
        final outToplevel = sourceOutputDir.get()
        final pdToplevel = projectOutputDir.get()
        final tklStrategy = testKitLocationsProvider.get()
        final pkg = pkgName.get()
        final depMsgMap = deprecationMessagesMapProperty.get().versionMap
        final dbg = useDebug.get()
        final dbgWarnings = debugWarnings.get()
        final ccMap = configurationCacheModeMapProperty.get()
        final dt = defaultTask.get()
        final cleanGradleCache = cleanCache.get()
        final args = arguments.get()
        final failures = expectedFailures.get()
        final uri = gradleDistributionURI.getOrNull()
        final manifest = manifestFileProvider.getOrNull()

        final ftds = testListProvider.get().collect { testDef ->
            final pd = new File(
                pdToplevel,
                "${testDef.gradleVersion}/${testDef.testDir.name}${postfixFromBuildFile(testDef.gradleBuildFile)}"
            )
            final tkd = testKitSubDir(testDef, tklStrategy, pdToplevel)
            new FullTestDefinition(testDef).tap {
                targetDir = outToplevel
                projectDir = pd
                testKitDir = tkd
                packageName = pkg
                deprecationWarnings = depMsgMap[testDef.gradleVersion]
                debug = configureDebug(testDef, dbg, dbgWarnings, ccMap)
                configurationCache = ccModeString(testDef, ccMap)
                defaultTask = dt
                cleanCache = cleanGradleCache
                arguments = args
                willFail = failures.any { pat -> testDef.testDir.name =~ pat }
                manifestFile = (manifest ? Optional.of(manifest) : Optional.empty()) as Optional<File>
                gradleDistributionUri = (uri ?
                    Optional.of(uri.resolve("gradle-${testDef.gradleVersion}-bin.zip").toString()) :
                    Optional.empty()) as Optional<String>

            }
        }

        templateDir.get().mkdirs()
        final testTemplate = loadTemplateFile()
        final traitsTemplate = loadTraitsTemplateFile()
        final makeCopies = copyOverSymlink.get()
        final List<File> generatedFiles = []
        ftds.each {
            generatedFiles.add(GenerationUtils.generateTest(outToplevel, testTemplate, fsOperations(), it))
            GenerationUtils.generateProject(it.projectDir, it, makeCopies, fsOperations())
        }

        generatedFiles.add(GenerationUtils.generateTraitsFile(outToplevel, traitsTemplate, fsOperations(), pkg))
        GenerationUtils.removeOldGeneratedFiles(outToplevel, pkg, generatedFiles)
    }

    private File loadTemplateFile() {
        final template = new File(templateDir.get(), 'GradleTestTemplateV4.groovy.template')
        fsOperations().copyResourceToFile('org/ysb33r/gradletest/GradleTestTemplateV4.groovy.template', template)
        template
    }

    private File loadTraitsTemplateFile() {
        final template = new File(templateDir.get(), 'GradleTestTraits.groovy')
        fsOperations().copyResourceToFile('org/ysb33r/gradletest/GradleTestTraits.groovy', template)
        template
    }

    private String postfixFromBuildFile(String buildFileName) {
        switch (buildFileName) {
            case GROOVY.buildFilePattern:
                return ''
            case KOTLIN.buildFilePattern:
                return '_kts'
            default:
                return buildFileName.toLowerCase(Locale.US).replaceAll(~/\./, '_')
        }
    }

    private File testKitSubDir(
        BasicTestDefinition btd,
        TestKitLocations tkl,
        File projectTopLevel
    ) {
        switch (tkl) {
            case SHARED:
                return new File(projectTopLevel, TESTKIT_PROJECT_LEVEL_SUBDIR)
            case PER_TEST:
                return new File(
                    projectTopLevel,
                    "${TESTKIT_PROJECT_LEVEL_SUBDIR}/${btd.gradleVersion}/${btd.testDir.name}${postfixFromBuildFile(btd.gradleBuildFile)}"
                )
            case PER_GROUP:
                return new File(
                    projectTopLevel,
                    "${TESTKIT_PROJECT_LEVEL_SUBDIR}/${btd.testDir.name}"
                )
        }

    }

    private String ccModeString(
        BasicTestDefinition btd,
        Map<String, ConfigurationCacheMode> ccMode
    ) {
        final cc = ccMode[btd.gradleVersion]
        if (GradleVersion.version(btd.gradleVersion) < MINIMUM_SUPPORTED_FOR_CONFIGURATION_CACHE) {
            if (cc != ConfigurationCacheMode.NONE) {
                logger.warn("Configuration cache will not be used because Gradle version is ${btd.gradleVersion}")
                ConfigurationCacheMode.NONE.mode
            } else {
                cc.mode
            }
        } else {
            cc.mode
        }
    }

    @SuppressWarnings('LineLength')
    private boolean configureDebug(
        BasicTestDefinition btd,
        boolean defaultMode,
        boolean warningMode,
        Map<String, ConfigurationCacheMode> ccMap
    ) {
        if (defaultMode) {
            if (btd.gradleBuildFile.endsWith('.kts') &&
                GradleVersion.version(btd.gradleVersion) < MINIMUM_SUPPORTED_KOTLIN_DEBUG) {
                final msg = "Kotlin DSL debug is not available for Gradle version ${btd.gradleVersion} on ${btd.testDir.name}"
                logDbgWarning(warningMode, msg)
                false
            } else if (ccMap[btd.gradleVersion] != ConfigurationCacheMode.NONE &&
                GradleVersion.version(btd.gradleVersion) >= MINIMUM_SUPPORTED_CONFIGURATION_CACHE_DEBUG) {
                final msg = "Debug cannot be used on Gradle version ${btd.gradleVersion} due to configuration-cache"
                logDbgWarning(warningMode, msg)
                false
            } else {
                true
            }
        } else {
            false
        }
    }

    private void logDbgWarning(boolean warningMode, String msg) {
        if (warningMode) {
            logger.warn(msg)
        } else {
            logger.debug(msg)
        }
    }

    private void configureInputs() {
        inputs.tap {
            property('pkgName', this.pkgName)
            property('debug', this.useDebug)
            property('ccMode', this.configurationCacheModeMapProperty)
            property('defaultTask', this.defaultTask)
            property('args', this.arguments)
            property('testkit', this.testKitLocationsProvider)
            property('testMap', this.testListProvider)
            property('warnings', this.deprecationMessagesMapProperty)
            property('failures', this.expectedFailures)
            file(this.manifestFileProvider).optional()
        }
    }

    private void configureConditionalExecution() {
        final ref = testListProvider
        onlyIf { !ref.get().empty }
    }

    private final Property<File> testSourceDirProvider
    private final Property<File> sourceOutputDirProvider
    private final Property<File> projectOutputDirProvider
    private final Property<TestKitLocations> testKitLocationsProvider
    private final Property<String> pkgName
    private final Property<Boolean> useDebug
    private final Property<Boolean> debugWarnings
    private final Property<Boolean> cleanCache
    private final Property<Boolean> copyOverSymlink
    private final Property<String> defaultTask
    private final ListProperty<BasicTestDefinition> testListProvider
    private final ListProperty<String> arguments
    private final ListProperty<Pattern> expectedFailures
    private final MapProperty<String, ConfigurationCacheMode> configurationCacheModeMapProperty
    private final Property<DeprecationMessagesMap> deprecationMessagesMapProperty
    private final Property<URI> gradleDistributionURI
    private final Provider<File> templateDir
    private final Property<File> manifestFileProvider

    private static final String SLASH = '/'
}

/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Carries all of the data for a test definition in order to generate data files.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
@ToString(includeNames = true, includeSuperFields = true, includeSuperProperties = true)
@EqualsAndHashCode
class FullTestDefinition extends BasicTestDefinition {
    FullTestDefinition(BasicTestDefinition btd) {
        super(
            testDir: btd.testDir,
            gradleBuildFile: btd.gradleBuildFile,
            gradleVersion: btd.gradleVersion
        )
    }

    /**
     * Directory where source is generated into
     */
    File targetDir

    /**
     * Directory where project will be executed from
     */
    File projectDir

    /**
     * Directory where test kit data must be written to if it is shared between projects.
     */
    File testKitDir

    /**
     * Name of the test package
     */
    String packageName

    /**
     * Configuration cache setting.
     *
     * Can be empty, 'fail' or 'warn'
     */
    String configurationCache

    /**
     * List of deprecation warnings to fail on. If empty, no checks will be performed.
     */
    List<String> deprecationWarnings

    /**
     * List of arguments to be passed to the Gradle process
     */
    List<String> arguments = []

//    isKotlinTest ? false : testPrep.withDebug,
    //                    // See Gradle bug https://github.com/gradle/kotlin-dsl/issues/1261
//                    // See Gradle bug https://github.com/gradle/gradle/issues/6862
    /**
     * Whether the test should enable debugging.
     */
    Boolean debug

    /**
     * This test is expected to fail.
     */
    Boolean willFail

    /**
     * Whether the Gradle local cache should be cleaned before the test starts
     */
    Boolean cleanCache

    /**
     * Whether a custom manifest file should be used.
     */
    Optional<File> manifestFile

    /**
     * URI to load distributions from.
     */
    Optional<String> gradleDistributionUri

    /**
     * The name of the task that needs to be executed
     */
    String defaultTask
}

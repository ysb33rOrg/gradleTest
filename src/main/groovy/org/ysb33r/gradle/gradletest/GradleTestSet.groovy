/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.Named
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.gradletest.internal.TestSet
import org.ysb33r.gradle.gradletest.internal.VersionExecutionProperties
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

import javax.inject.Inject
import java.util.function.Function
import java.util.function.Predicate
import java.util.regex.Pattern

import static java.util.Collections.EMPTY_LIST
import static org.ysb33r.gradle.gradletest.internal.TestSet.baseName
import static org.ysb33r.gradle.gradletest.internal.TestSet.testVersionsFromOutside

/**
 * Defines a GradleTest test set.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
@SuppressWarnings('MethodCount')
class GradleTestSet implements Named {

    public static final List<String> DEFAULT_DEPRECATION_WARNINGS = [
        /The .+ (property|method) has been deprecated .+ scheduled to be removed in Gradle/,
        /Gradle now uses separate output directories for each JVM language, but this build assumes a / +
            /single directory for all classes from a source set/,
        /The .+ configuration has been deprecated for dependency declaration/,
        /.+This will fail with an error in Gradle.+/
    ].asImmutable()

    final String name

    @Inject
    @SuppressWarnings('AbcMetric')
    GradleTestSet(String name, GradleTestSetExtension parent, Project project) {
        final basename = baseName(name)
        this.objectFactory = project.objects
        this.name = name
        this.versionsMap = [:]
        this.deprecationWarnings = []
        this.deprecationWarnings.addAll(DEFAULT_DEPRECATION_WARNINGS)
        this.warningsByVersion = [:]
        this.arguments = []
        this.versionProvider = project.provider { -> versionsMap.keySet() }
        this.ccso = ConfigCacheSafeOperations.from(project)
        this.ccModeLookup = [{ String ver -> ConfigurationCacheMode.NONE } as Function<String, ConfigurationCacheMode>]
        this.deprecationWarningsPredicate = [{ String ver -> true } as Predicate<String>]
        this.sourceDir = project.objects.property(File)
        this.genDir = project.objects.property(File)
        this.testKitStrategy = project.objects.property(TestKitLocations)
        this.useDebug = project.objects.property(Boolean)
        this.cleanCache = project.objects.property(Boolean)
        this.preferCopyOverSymlink = project.objects.property(Boolean)
        this.defaultTaskName = project.objects.property(String)
        this.expectedFailures = project.objects.listProperty(Pattern)
        this.gradleDistributionURI = project.objects.property(URI)
        this.argumentsProvider = project.provider { -> ccso.stringTools().stringizeDropNull(owner.arguments) }
        this.packageName = parent.testPackage.map { "${it}.${basename.toLowerCase(Locale.US)}".toString() }
        this.deprecationMessagesAreFailuresForAllVersions = true

        this.useDebug.set(false)
        this.preferCopyOverSymlink.set(false)
        this.cleanCache.set(true)
        this.ccModeProvider = project.provider { ->
            versionsMap.collectEntries { k, v ->
                [k, v.ccMode]
            } as Map<String, ConfigurationCacheMode>
        }

        sourceDirectory = "src/${basename}"
        genDir.set(project.layout.projectDirectory.dir(TestSet.getRelativeSourceDir(basename)).asFile)
        defaultTaskName.set('runGradleTest')

        final versFromOutside = testVersionsFromOutside(baseName(name), ccso.providerTools()).get()

        this.ignoreConfiguredVersions = !versFromOutside.empty
        this.versionsMap.putAll(versFromOutside.collectEntries { [it, new VersionExecutionProperties()] })
        this.warningsProvider = project.provider { ->
            new DeprecationMessagesMap().tap {
                versionMap.putAll(
                    versionsMap.collectEntries { k, v ->
                        [k, v.deprecationWarningsMap]
                    }
                )
            }
        }

        testKitDirectoryShared()
        updateVersionExecutionProperties()
    }

    /**
     * By default, project structures are symlinked back to the files in the project source directory.
     * This saves a lot of disk space and speeds things up.
     * This can cause issues if the test writes anything back into directory that was symlinked.
     * This behaviour can be turned off, so that everything is copied.
     *
     * <p>
     *     When turned off, directories are copied, but single files at the toplevel of the project are still symlinked.
     * </p>
     */
    void copyNotSymlink() {
        this.preferCopyOverSymlink.set(true)
    }

    /**
     * Whether copy should be preferred over symlinks.
     *
     * <p>
     *     The default is to use symlinks.
     * </p>
     *
     * @return Provider that indicates the preference.
     */
    Provider<Boolean> getCopyOverSymlink() {
        this.preferCopyOverSymlink
    }

    /**
     * Whether the Gradle cache should be cleaned before each test tun..
     *
     * <p>
     *     The default is to clean the cache.
     *     Not cleaning the cache results in slightly faster runs, but can have hard-to-diagnose issues.
     * </p>
     *
     * @return
     */
    Provider<Boolean> getCleanCache() {
        this.cleanCache
    }

    /**
     * Whether cache should be clean before each test run.
     *
     *
     * @param flag {@code false} turns cache cleaning off..
     */
    void setCleanCache(boolean flag) {
        this.cleanCache.set(flag)
    }

    /**
     * A provider to indicate debug mode.
     *
     * @return
     */
    Provider<Boolean> getDebug() {
        this.useDebug
    }

    /**
     * Whether debug should be enabled for tests.
     *
     * <p>
     *     Even if debug is et, for certain tests, it might remain disabled, especially for older version of Gradle
     *     with Kotlin DSL, or for configuration cache.
     * </p>
     *
     * @param flag {@code true} turns debug on.
     */
    void setDebug(boolean flag) {
        this.useDebug.set(flag)
    }

    /**
     * Name of the task to run inside the Gradle test project.
     *
     * <p>
     *     The default is {@code runGradleTest}.
     * </p>
     *
     * @return Task name
     */
    Provider<String> getDefaultTaskToRun() {
        this.defaultTaskName
    }

    /**
     * Overrides the default task to run inside the Gradle test project.
     *
     * @param taskName New task name
     */
    void setDefaultTaskToRun(String taskName) {
        this.defaultTaskName.set(taskName)
    }

    /**
     * Get an alternative location where to get Gradle distributions from.
     *
     * <p>
     *     The location is expected to store Gradle packages in the standard format.
     * </p>
     *
     * @return Location of Gradle distributions. Can be empty.
     */
    Provider<URI> getGradleDistributionURI() {
        this.gradleDistributionURI
    }

    /**
     * Sets the location of where to fetch Gradle distributions from.
     *
     * @param uri Location URI. Can be a string or a File, or anything that can be converted to a URI.
     */
    void setGradleDistributionURI(Object uri) {
        this.gradleDistributionURI.set(ccso.providerTools().provider { ->
            ccso.stringTools().urize(uri)
        })
    }

    /**
     * Returns the name of the test task associated with this test set.
     *
     * @return Task name
     */
    String getTestTaskName() {
        baseName(name)
    }

    /**
     * Returns the name of the task that will generate source for this test set.
     *
     * @return Task name
     */
    String getTestSourceGeneratorTaskName() {
        TestSet.getGeneratorTaskName(baseName(name))
    }

    /**
     * Returns the name of the task that will generate the Gradle manifest for this test set.
     *
     * <p>
     *     This will return a name, but this does no guarantee that it will be used.
     *     In order for it to be used, {@link #useCustomManifest} has to be called.
     * </p>
     *
     * @return Task name
     */
    String getCustomTestManifestTaskName() {
        TestSet.getManifestTaskName(baseName(name))
    }

    /**
     * Indicate that a custom manifest file must be used.
     *
     * <p>
     *     This will use a custom manifest, even if the {@code java-gradle-plugin} has been applied.
     *     It will use the runtime classpath for the source set of this test set.
     *     Additional dependencies can be added to the {@code runtimeOnly} configuration of the source set associated
     *     with this testset.
     *     This will be {@code gradleTestRuntimeOnly} for the {@code main} testset or something like
     * {@code myExtraGradleTestRuntimeOnly} for a test set called {@code myExtra}.
     * </p>
     *
     * <p>
     *     This also adds a task called {@code gradleTestClasspathManifest}.
     *     For a test set called {@code myExtra}, it will be called {@code myExtraGradleTestClasspathManifest}.
     * </p>
     */
    void useCustomManifest() {
        objectFactory.newInstance(TestSet.ManifestRegistrar, this).maybeRegister()
    }

    /**
     * Additional arguments to pass to gradle when running the test.
     *
     * @return Provider to additional arguments
     */
    Provider<List<String>> getGradleArguments() {
        this.argumentsProvider
    }

    /**
     * Additional Gradle arguments to pass.
     *
     * @param args Command-line arguments.
     */
    void gradleArguments(Object... args) {
        this.arguments.addAll(args)
    }

    /**
     * Additional Gradle arguments to pass.
     *
     * @param args Command-line arguments.
     */
    void gradleArguments(List<Object> args) {
        this.arguments.addAll(args)
    }

    /**
     * Returns a map for every testable version which indicates whether deprecation messages should be checked
     * for that version.
     *
     * <p>
     *     If deprecation warnings should not be checked the list of test patterns will be empty, otherwise
     *     it will contain a list of regex patterns.
     * </p>
     *
     * @return Deprecation messages check.
     */
    Provider<DeprecationMessagesMap> getDeprecationMessagesAreFailures() {
        this.warningsProvider
    }

    /**
     * Set whether deprecation message should fail the build.
     *  <p>
     *      Overrides previous calls to {@link #setDeprecationMessagesAreFailuresForAllVersions}.
     *  </p>
     *
     * @param forVersions Indicates for which Gradle test versions, deprecation versions should be checked.
     */
    void setDeprecationMessagesAreFailures(Predicate<String> forVersions) {
        deprecationWarningsPredicate[0] = forVersions
        updateVersionExecutionProperties()
    }

    /**
     * Turns deprecation checking on or off for all Gradle versions.
     *  <p>
     *      Overrides previous calls to {@link #setDeprecationMessagesAreFailures}.
     *  </p>
     *
     * @param flag {@code true} turns on warnings check for all versions.
     */
    void setDeprecationMessagesAreFailuresForAllVersions(Boolean flag) {
        deprecationMessagesAreFailures = (flag ? { true } : { false }) as Predicate<String>
        updateVersionExecutionProperties()
    }

    /**
     * Returns the list of default deprecation messages.
     *
     * <p>
     * This list can be modified and messages added or removed.
     * </p>
     *
     * @return Message list
     */
    List<String> getDefaultDeprecationMessageChecks() {
        this.deprecationWarnings
    }

    /**
     * Replace the set of default deprevation messages.
     *
     * @param msgPatterns New set of patterns.
     */
    void setDefaultDeprecationMessageChecks(Collection<String> msgPatterns) {
        this.deprecationWarnings.clear()
        this.deprecationWarnings.addAll(msgPatterns)
    }

    /**
     * Add deprecation message patterns to the default message sets.
     *
     * @param msgPatterns Additional set of patterns
     */
    void defaultDeprecationMessageChecks(String... msgPatterns) {
        this.deprecationWarnings.addAll(msgPatterns)
    }

    /**
     * Add deprecation message patterns to the default message sets.
     *
     * @param msgPatterns Additional set of patterns
     */
    void defaultDeprecationMessageChecks(Collection<String> msgPatterns) {
        this.deprecationWarnings.addAll(msgPatterns)
    }

    /**
     * Modifies the deprecation messages this for a specific Gradle version.
     *
     * <p>
     *     Once this method is called, even if no mods are made, the specific Gradle version will no longer
     *     use the default set.
     * <p>
     * @param version Gradle version to customise
     * @return List of messages that can be modified.
     */
    void deprecationMessageChecksForVersion(String version, Collection<String> msgPatterns) {
        final msgs = this.warningsByVersion[version]
        if (msgs) {
            msgs.addAll(msgPatterns)
            updateVersionExecutionProperties()
        } else {
            replaceDeprecationMessageChecksForVersion(version, msgPatterns)
        }
    }

    /**
     * Modifies the deprecation messages this for a specific Gradle version.
     *
     * <p>
     *     Once this method is called, even if no mods are made, the specific Gradle version will no longer
     *     use the default set.
     * <p>
     * @param version Gradle version to customise
     * @return List of messages that can be modified.
     */
    void deprecationMessageChecksForVersion(String version, String... msgPatterns) {
        deprecationMessageChecksForVersion(version, msgPatterns.toList())
    }

    /**
     * Initialises a set of messages  for a version vy copying the current default set and then
     * adding additional set.
     *
     * <p>
     *     Once this method is called, even if no mods are made, the specific Gradle version will no longer
     *     use the default set.
     * <p>
     * @param version Gradle version to customise
     * @return List of messages that can be modified.
     */
    void augmentMessageChecksForVersionFromDefault(String version, String... msgPatterns) {
        augmentMessageChecksForVersionFromDefault(version, msgPatterns.toList())
    }

    /**
     * Initialises a set of messages  for a version vy copying the current default set and then
     * adding additional set.
     *
     * <p>
     *     Once this method is called, even if no mods are made, the specific Gradle version will no longer
     *     use the default set.
     * <p>
     * @param version Gradle version to customise
     * @return List of messages that can be modified.
     */
    void augmentMessageChecksForVersionFromDefault(String version, Collection<String> msgPatterns) {
        final msgs = this.warningsByVersion[version]
        if (msgs) {
            msgs.clear()
            msgs.addAll(this.deprecationWarnings)
            msgs.addAll(msgPatterns)
        } else {
            final List<String> addition = []
            addition.addAll(msgPatterns)
            addition.addAll(this.deprecationWarnings)
            this.warningsByVersion.put(version, addition)
        }
        updateVersionExecutionProperties()
    }

    /**
     * Replaces the current set of messages for a specific version with a new set..
     *
     * <p>
     *     Once this method is called, even if no mods are made, the specific Gradle version will no longer
     *     use the default set.
     * <p>
     *
     * @param version Gradle version to customise
     * @return List of messages that can be modified.
     */
    void replaceDeprecationMessageChecksForVersion(String version, Collection<String> msgPatterns) {
        final msgs = this.warningsByVersion[version]
        if (msgs) {
            msgs.clear()
            msgs.addAll(msgPatterns)
        } else {
            final List<String> addition = []
            addition.addAll(msgPatterns)
            this.warningsByVersion.put(version, addition)
        }
        updateVersionExecutionProperties()
    }

    /**
     * Test groups which are expected to fail.
     *
     * @return List of patterns to match against test names.
     */
    Provider<List<Pattern>> getExpectedFailures() {
        this.expectedFailures
    }

    /**
     * Set which tests are expected to fail.
     *
     * @param patterns Regex patterns. These should match the name of the GradleTest folder i.e.
     *   for {@code src/gradleTest/myTest},
     *   then it should try to match on {@code myTest}.
     */
    void expectedFailures(String... patterns) {
        this.expectedFailures.addAll(patterns.collect { Pattern.compile(it) })
    }

    /**
     * Set which tests are expected to fail.
     *
     * @param patterns Regex patterns. These should match the name of the GradleTest folder i.e.
     *   for {@code src/gradleTest/myTest},
     *   then it should try to match on {@code myTest}.
     */
    void expectedFailures(Pattern... patterns) {
        this.expectedFailures.addAll(patterns)
    }

    /**
     * A strategy for storing TestKit related data.
     *
     * The default is to discard testkit data after tests.
     */
    Provider<TestKitLocations> getTestKitStrategy() {
        this.testKitStrategy
    }

    /**
     * Shares the testkit directory between all tests.
     *
     * Tests run slightly faster, but problems can be harder to diagnose.
     */
    void testKitDirectoryShared() {
        this.testKitStrategy.set(TestKitLocations.SHARED)
    }

    /**
     * Keep the Testkit data, but share the directory between the complete testset.
     */
    void testKitDirectoryPerGroup() {
        this.testKitStrategy.set(TestKitLocations.PER_GROUP)
    }

    /**
     * Keep one TestKit directory per test.
     *
     * This options uses the most space, but allows detailed analysis per test.
     */
    void testKitDirectoryPerTest() {
        this.testKitStrategy.set(TestKitLocations.PER_TEST)
    }

    /**
     * The root directory where to find tests for this specific GradleTest grouping
     * The default root directory by convention is {@code src/gradleTest}. The patterns for the
     * directory is {@code src/} + {@code gradleTestSetName}.
     *
     * @return The directory as a file object resolved as per {@code project.file ( )}.
     */
    Provider<File> getSourceDirectory() {
        this.sourceDir
    }

    /**
     * Overrides the default location of test project sources.
     *
     * @param src The location of the gradle test project sources.
     */
    void setSourceDirectory(Object src) {
        ccso.fsOperations().updateFileProperty(this.sourceDir, src)
    }

    /**
     * The top-level directory for sources will be generated into for this source set.
     *
     * @return Output directory
     */
    Provider<File> getGeneratedSourceDir() {
        this.genDir
    }

    /**
     * Override the directory where sources are generated into.
     *
     * @param out Target directory
     */
    void setGeneratedSourceDir(Object out) {
        ccso.fsOperations().updateFileProperty(this.genDir, out)
    }

    /**
     * The package to be used for tests in this set.
     *
     * @return Package name
     */
    Provider<String> getPackage() {
        this.packageName
    }

    /**
     * Sets a mapper that determines which Gradle versions should active which kind of configuration cache checks.
     *
     * @param mapper Gradle version in, cache-mode out.
     */
    void configurationCacheMapping(Function<String, ConfigurationCacheMode> mapper) {
        this.ccModeLookup[0] = mapper
        updateVersionExecutionProperties()
    }

    /**
     * Like {@link #configurationCacheMapping}, but returns a case-insensitive string value which can then be converted
     * by the test set.
     *
     * <p>
     *     This makes it easier to use in a DSL.
     * </p>
     *
     * @param mapper Gradle version in, cache-mode string value out.
     */
    void configurationCacheMappingByAlias(Function<String, String> mapper) {
        this.ccModeLookup[0] = mapper.andThen { ConfigurationCacheMode.valueOf(it.toUpperCase(Locale.US)) }
        updateVersionExecutionProperties()
    }

    /**
     * A provider that maps configuration cache setup.
     *
     * @return A mapping for each Gradle version.
     */
    Provider<Map<String, ConfigurationCacheMode>> getConfigurationCacheModeMapping() {
        this.ccModeProvider
    }

    /**
     * Returns the set of Gradle versions to tests against.
     *
     * <p>
     *     If a Gradle or system property similar to {@code gradleTest.versions} or an environment variable similar to
     * {@code GRADLETEST_VERSIONS} is specified, it will return the versions parsed from that instead.
     * </p>
     *
     * @return Set of unique versions.
     */
    Provider<Set<String>> getVersions() {
        this.versionProvider
    }

    /**
     * Add Gradle versions to be tested against.
     *
     * @param ver List of versions
     */
    void versions(Object... ver) {
        if (!ignoreConfiguredVersions) {
            addVersions(ver.toList())
            updateVersionExecutionProperties()
        }
    }

    /**
     * Add Gradle versions to be tested against.
     *
     * @param ver List of versions
     *   The values are evaluated immediately.
     */
    void versions(Iterable<Object> ver) {
        if (!ignoreConfiguredVersions) {
            addVersions(ver.toList())
            updateVersionExecutionProperties()
        }
    }

    /**
     * Replace all previously defined versions.
     *
     * @param ver List of versions.
     *   The values are evaluated immediately.
     */
    void setVersions(Collection<Object> ver) {
        if (!ignoreConfiguredVersions) {
            this.versionsMap.clear()
            addVersions(ver)
            updateVersionExecutionProperties()
        }
    }

    private void addVersions(Collection<Object> ver) {
        ccso.stringTools().stringize(ver).each {
            this.versionsMap.putIfAbsent(it, new VersionExecutionProperties())
        }
    }

    private void updateVersionExecutionProperties() {
        this.versionsMap.each { k, v ->
            v.ccMode = owner.ccModeLookup[0].apply(k)

            final checkWarnings = this.deprecationWarningsPredicate[0].test(k)
            final correctList = checkWarnings ? (
                this.warningsByVersion.containsKey(k) ? this.warningsByVersion[k] : this.deprecationWarnings
            ) : EMPTY_LIST
            v.deprecationWarningsMap = correctList

            // TODO: what about ignoring a test if there is a rule for it.
        }
    }

    private final ObjectFactory objectFactory
    private final Map<String, VersionExecutionProperties> versionsMap
    private final Provider<Set<String>> versionProvider
    private final ConfigCacheSafeOperations ccso
    private final Property<File> sourceDir
    private final Property<File> genDir
    private final Property<Boolean> useDebug
    private final Property<Boolean> cleanCache
    private final Property<Boolean> preferCopyOverSymlink
    private final Property<TestKitLocations> testKitStrategy
    private final Provider<String> packageName
    private final Property<String> defaultTaskName
    private final Property<URI> gradleDistributionURI
    private final List<String> deprecationWarnings
    private final Map<String, List<String>> warningsByVersion
    private final Provider<DeprecationMessagesMap> warningsProvider

    private final Function<String, ConfigurationCacheMode>[] ccModeLookup
    private final Provider<Map<String, ConfigurationCacheMode>> ccModeProvider

    private final ListProperty<Pattern> expectedFailures
    private final List<Object> arguments
    private final Provider<List<String>> argumentsProvider

    private final Predicate<String>[] deprecationWarningsPredicate
    private final Boolean ignoreConfiguredVersions
}

/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

/**
 * Generates a manifest file that can be re-used by {@code GradleTest} tasks to set the correct classpath.
 *
 * @since 1.0
 */
@CompileStatic
class ClasspathManifest extends GrolifantDefaultTask {

    ClasspathManifest() {
        this.outputFile = project.objects.property(File)
        this.classpath = fsOperations().emptyFileCollection()

        inputs.files(this.classpath)
    }

    /**
     * Returns manifest file location.
     *
     * @return Provider to output file
     */
    @OutputFile
    Provider<File> getOutputFile() {
        this.outputFile
    }

    /**
     * Sets the output directory.
     *
     * @param out Output directory provider
     *
     * @since 4.0
     */
    void setOutputDir(Provider<File> out) {
        this.outputFile.set(out.map { new File(it, 'manifest.txt') })
    }

    /**
     * Access to the classpath that will be placed inside the manifest
     *
     * @return Classpath that can be configured.
     */
    @Classpath
    ConfigurableFileCollection getClasspath() {
        this.classpath
    }

    @TaskAction
    void exec() {
        outputFile.get().text = classpath.files*.absolutePath.join('\n')
    }

    private final Property<File> outputFile
    private final ConfigurableFileCollection classpath
}

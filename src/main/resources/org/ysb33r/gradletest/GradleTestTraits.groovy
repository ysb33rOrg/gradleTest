/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package ____TESTPACKAGE____

import groovy.transform.CompileStatic
import org.gradle.testkit.runner.GradleRunner
import org.gradle.util.GradleVersion

@CompileStatic
trait GradleTestTraits {

    GradleRunner getGradleRunner() {
        final runner = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .forwardOutput()
            .withDebug(withDebug)
            .withTestKitDir(testKitDir)

        if (distributionUriAsString.empty) {
            runner.withGradleVersion(gradleVersionUnderTestString)
        } else {
            runner.withGradleDistribution(distributionUriAsString.toURI())
        }

        if (customManifestAvailable) {
            final pluginClasspath = customManifestLocation.readLines().collect {
                new File(it)
            }
            runner.withPluginClasspath(pluginClasspath)
        } else {
            runner.withPluginClasspath()
        }

        runner.withArguments(getArguments(gradleVersionUnderTest))
    }

    GradleVersion getGradleVersionUnderTest() {
        GradleVersion.version(gradleVersionUnderTestString)
    }

    List<String> getArguments(final GradleVersion gradleVersionUnderTest) {
        List<String> args = [defaultTaskName]
        args.addAll(gradleArguments)
        args.addAll('--console=plain', '-Porg.gradle.daemon=false')

        if (gradleVersionUnderTest >= GradleVersion.version('4.5')) {
            args.addAll('--warning-mode=all')
        }

        if (gradleVersionUnderTest >= GradleVersion.version('6.6')) {
            final cc = configurationCacheValue
            if (cc.empty) {
                args.add('--no-configuration-cache')
            } else {
                args.addAll('--configuration-cache', "--configuration-cache-problems=${cc}".toString())
            }
        }
        args
    }

    boolean no_deprecation_warnings(final String output) {
        if (deprecationWarnings.empty) {
            true
        } else {
            !deprecationWarnings.any { pat ->
                output =~ pat
            }
        }
    }

    void cleanCacheDirBeforeStart() {
        if(cleanCacheDir) {
            File cacheDir = new File(testProjectDir,'.gradle')
            if(cacheDir.exists()) {
                cacheDir.deleteDir()
            }
        }
    }

    // TODO: Not sure this still get used properly
    String getDistributionFilename() {
        String fname = '${DIST_FILENAME_PATTERN}'.replaceAll('@version@', gradleVersionUnderTestString)

        if (!fname.startsWith("/")) {
            "/" + fname
        } else {
            fname
        }
    }

    Boolean getCustomManifestAvailable() {
        customManifestLocation != null
    }

    abstract Boolean getWithDebug()

    abstract Boolean getCleanCacheDir()

    abstract File getTestProjectDir()

    abstract File getCustomManifestLocation()

    abstract File getTestKitDir()

    abstract String getDistributionUriAsString()

    abstract String getGradleVersionUnderTestString()

    abstract String getConfigurationCacheValue()

    abstract String getDefaultTaskName()

    abstract List<String> getGradleArguments()

    abstract List<String> getDeprecationWarnings()
}
/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.ysb33r.gradle.gradletest.internal.GradleTestIntegrationSpecification
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class CompilationIntegrationSpec extends GradleTestIntegrationSpecification {

    public static final String RUNTIME_LIST_FILE = 'runtime-list.txt'
    public static final String WRITE_ARTIFACTS_TASK = 'writeArtifacts'

    File srcDir

    void setup() {
        srcDir = new File(projectDir, 'src/gradleTest')
        srcDir.mkdirs()
    }

    @Unroll
    void 'Compile generated code using Gradle #gradleVer'() {
        setup:
        configureGradleTest()
        genTestStructure()

        when:
        final result = getGradleRunner([
            'gradleTestClasses',
            WRITE_ARTIFACTS_TASK,
            '--console=plain',
            '-s'
        ]).withGradleDistribution(getGradleDistributionUriFor(gradleVer))
            .build()

        final targetVer = oneOfEachMainGradleReleases()[0]
        final testFor = "classes/groovy/gradleTest/test/ysb33r/gradle/compatibility/gradletest/Alpha__${targetVer.replaceAll('\\.', '_')}Spec.class"

        then:
        result.task(':gradleTestGenerator').outcome == SUCCESS
        result.task(':compileGradleTestGroovy').outcome == SUCCESS
        result.task(':gradleTestClasses').outcome == SUCCESS
        result.task(":${WRITE_ARTIFACTS_TASK}").outcome == SUCCESS
        new File(buildDir, testFor).exists()

        when:
        final runtime = loadTextFile(new File(buildDir, RUNTIME_LIST_FILE))

        then:
        runtime*.name.findAll { it.startsWith('groovy-3') }.size() == 1

        where:
        gradleVer << runnableGradleReleases()
    }

    private List<File> loadTextFile(File path) {
        path.readLines().toList().collect { new File(it) }
    }

    private void genTestStructure() {
        File testDir = new File(srcDir, 'alpha')
        testDir.mkdirs()
        new File(testDir, 'build.gradle').text = '''
            task runGradleTest  {
                doLast {
                }
            }
        '''.stripIndent()
    }

    private void configureGradleTest() {
        writeBuildScriptHeader()
        buildFile << """

        gradleTestSets {
            testSets {
              main {
                versions ${oneOfEachMainGradleReleasesAsDslString()}
                gradleDistributionURI = '${GRADLETESTREPO.toURI()}'

                defaultDeprecationMessageChecks = [
                    /Some pattern matching dot character: \\./,
                    /Some pattern matching any whitespace character: \\s/,
                ]
              }
            }
        }

         tasks.register ('${WRITE_ARTIFACTS_TASK}') {   
            final fc = configurations.getByName('gradleTestRuntimeClasspath')
            final out = project.file('build/${RUNTIME_LIST_FILE}')

            doLast {
                out.withWriter { w ->
                    fc.files.each { w.println it.absolutePath }
                }
            }
        }
        """.stripIndent()
    }
}
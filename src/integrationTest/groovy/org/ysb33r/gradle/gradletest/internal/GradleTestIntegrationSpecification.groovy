/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import org.gradle.testkit.runner.GradleRunner
import org.gradle.util.GradleVersion
import org.ysb33r.grolifant5.api.core.Version
import spock.lang.Specification
import spock.lang.TempDir

/**
 * Before running anything from here, ensure that `gradle createIntegrationTestClasspathManifest` has been run.
 */
class GradleTestIntegrationSpecification extends Specification {

    public static final List<String> AVAILABLE_GRADLE_VERSIONS = System.getProperty('AVAILABLE_GRADLE_VERSIONS').split(',').toList()
//    ['4.3.1', '4.10.3', '5.6.3', '6.0.1', '6.9.4', '7.6.1', '8.0.2', '8.1.1']
    public static final File GRADLETESTREPO = new File(System.getProperty('GRADLETESTREPO')).absoluteFile
    public static final String OFFLINE_REPO_DSL = new File(System.getProperty('org.ysb33r.ivypot.repo.groovy.dsl.file')).text

    @TempDir
    File testProjectDir

    File buildFile
    File projectDir
    File buildDir
    File settingsFile

    void setup() {
        projectDir = testProjectDir
        buildFile = new File(projectDir, 'build.gradle')
        buildDir = new File(projectDir, 'build')
        settingsFile = new File(projectDir, 'settings.gradle')
        settingsFile.text = 'rootProject.name = "integration-test-project"'
    }

    void writeBuildScriptHeader(List<String> plugins = []) {
        final extraPlugins = plugins.collect {
            ' ' * 16 + "id '${it}'"
        }.join('\n')
        buildFile << """
        import org.gradle.util.GradleVersion 

        plugins {
            id 'java'
            id 'org.ysb33r.gradletest'
            ${extraPlugins}
        }
        """.stripIndent() + OFFLINE_REPO_DSL

        settingsFile.text = ''
    }

    URI getGradleDistributionUriFor(String gradleVersion) {
        new File(GRADLETESTREPO, "distributions/gradle-${gradleVersion}-bin.zip").toURI()
    }

    GradleRunner getGradleRunner(List<String> args) {
        GradleRunner.create()
            .withProjectDir(projectDir)
            .withArguments(args)
            .forwardOutput()
            .withPluginClasspath()
            .withDebug(true)
    }

    GradleRunner getGradleRunnerConfigCache(List<String> args) {
        getGradleRunner(args + ['--configuration-cache', '--configuration-cache-problems=fail'])
            .withGradleVersion('8.9')
            .withDebug(false)
    }

    static List<String> allAvailableGradleReleases() {
        AVAILABLE_GRADLE_VERSIONS
    }

    static List<String> runnableGradleReleases() {
        AVAILABLE_GRADLE_VERSIONS.findAll { GradleVersion.version(it) >= GradleVersion.version('7.3') }
    }

    static List<String> oneOfEachMainGradleReleases() {
        runnableGradleReleases().groupBy {
            Version.of(it).major
        }.values()*.getAt(0)
    }

    static String oneOfEachMainGradleReleasesAsDslString(final String quoteCharacter = '\'') {
        oneOfEachMainGradleReleases().collect {
            "${quoteCharacter}${it}${quoteCharacter}"
        }.join(',')
    }

    static String allAvailableGradleReleasesAsDslString(final String quoteCharacter = '\'') {
        allAvailableGradleReleases().collect {
            "${quoteCharacter}${it}${quoteCharacter}"
        }.join(',')
    }

    static String runnableGradleReleasesAsDslString(final String quoteCharacter = '\'') {
        runnableGradleReleases().collect {
            "${quoteCharacter}${it}${quoteCharacter}"
        }.join(',')
    }
}

/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2017 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.internal.GradleTestIntegrationSpecification
import spock.lang.Issue
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.gradle.testkit.runner.TaskOutcome.UP_TO_DATE
import static org.ysb33r.gradle.gradletest.internal.GenerationUtils.TESTKIT_PROJECT_LEVEL_SUBDIR

// Both println & these complex strings are correct within this context
@SuppressWarnings(['Println', 'GStringExpressionWithinString'])
class GradleTestIntegrationSpec extends GradleTestIntegrationSpecification {
    static public final List<String> TESTNAMES = ['alpha', 'beta', 'gamma']
    static public final String TEST_PKG = 'test.ysb33r'
    static public final String TEST_CLASS = 'TestPlugin'

    File srcDir

    void setup() {
        srcDir = new File(projectDir, 'src/gradleTest')
        srcDir.mkdirs()

        writeBuildScriptHeader(['java-gradle-plugin'])
        writeGradleTestConfiguration()
        genSourceFiles()
    }

    @Unroll
    void "Running with testKitStrategy = #mode"() {
        setup:
        genTestStructureForSuccess(srcDir)

        buildFile << """
        gradleTestSets {
            testSets {
                main {
                    ${mode}()
                }
            }
        }
        """.stripIndent()

        when:
        println "I'm starting this test chain and I'm " + GradleVersion.current()
        final result = getGradleRunner([
            'gradleTest', '-s'
        ]).build()

        then:
        result.task(':gradleTest').outcome == SUCCESS

        and: "A testkit capture directory was created"
        new File("${buildDir}/gradleTest/${location}").exists()

        where:
        mode                       | location
        'testKitDirectoryShared'   | "${TESTKIT_PROJECT_LEVEL_SUBDIR}"
        'testKitDirectoryPerGroup' | "${TESTKIT_PROJECT_LEVEL_SUBDIR}/${TESTNAMES[0]}"
        'testKitDirectoryPerTest'  | "${TESTKIT_PROJECT_LEVEL_SUBDIR}/${allAvailableGradleReleases()[0]}/${TESTNAMES[1]}"
    }

    @Unroll
    void "Can use a custom manifest #cc configuration cache"() {
        setup:
        genTestStructureForSuccess(srcDir)

        buildFile << """
        gradleTestSets {
            testSets {
                main {
                    useCustomManifest()
                    ${ccMode ? 'gradleArguments \'-s\'' : ''}
                    configurationCacheMappingByAlias { v -> 'none' }
                }
            }
        }
        """.stripIndent()

        when:
        final result = (ccMode ?
            getGradleRunnerConfigCache(['gradleTest', '-s']) :
            getGradleRunner(['gradleTest'])
        ).build()

        then:
        result.task(':gradleTestClasspathManifest').outcome == SUCCESS
        result.task(':gradleTest').outcome == SUCCESS

        and: "A manifest file was created"
        new File("${buildDir}/gradleTest/manifest.txt").exists()

        where:
        cc | ccMode
        'without' | false
        'with' | true
    }

    void 'Can configure the configuration cache for a test set'() {
        setup:
        buildFile.text = ''
        writeBuildScriptHeader(['java-gradle-plugin'])
        writeGradleTestConfiguration(runnableGradleReleasesAsDslString())
        genTestStructureForSuccess(srcDir)

        buildFile << """
        gradleTestSets {
            testSets {
                main {
                    useCustomManifest()
                    configurationCacheMappingByAlias { v -> v.startsWith('8.') ? 'fail' : 'none' }
                }
            }
        }
        """.stripIndent()

        when:
        final result = getGradleRunner(['gradleTest', '-s', '-i']).build()

        then:
        result.task(':gradleTestClasspathManifest').outcome == SUCCESS
        result.task(':gradleTest').outcome == SUCCESS
    }

    @Issue("https://github.com/ysb33r/gradleTest/issues/52")
    @Unroll
    void 'Handle expected failure (using Gradle Version #gradleVer)'() {
        setup:
        buildFile << '''
        gradleTestSets {
            testSets {
                main {
                    expectedFailures 'gamma'
                }
            }
        }
        '''.stripIndent()

        genTestStructureForFailure()

        when:
        println "I'm starting this test chain and I'm " + GradleVersion.current()
        final result = getGradleRunner([
            'gradleTest', '-s', '-i'
        ]).withGradleDistribution(getGradleDistributionUriFor(gradleVer))
            .build()
        then:
        result.task(":gradleTest").outcome == SUCCESS

        where:
        gradleVer << oneOfEachMainGradleReleases()
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradleTest/-/issues/59')
    void "Adding folders will re-run test task"() {
        setup: "A GradleTest structure with three tests"
        final gradleRunner = getGradleRunner(['gradleTest', '-s'])
        genTestStructureForSuccess(srcDir)

        when: 'When executed'
        println "I'm starting this test chain and I'm " + GradleVersion.current()
        final result = gradleRunner.build()

        then: 'The tests execute successfully'
        result.task(":gradleTest").outcome == SUCCESS

        when: 'It is executed again'
        println "I'm running this test chain again and I'm " + GradleVersion.current()
        final result2 = gradleRunner.build()

        then: 'The task should be skipped'
        result2.task(":gradleTest").outcome == UP_TO_DATE

        when: 'An additional test is added'
        File testDir = new File(srcDir, 'delta')
        File deltaBuild = new File(testDir, 'build.gradle')
        testDir.mkdirs()
        deltaBuild.text = '''
            task runGradleTest  {
                ext {
                    gVer = gradle.gradleVersion
                    pName = project.name
                }
                doLast {
                    println "DELTA: I'm runGradleTest and I'm " + gVer
                    println "DELTA: Hello, ${pName}"
                }
            }
'''
        and: 'The task is executed again'
        final result3 = gradleRunner.build()

        then: 'the task will be successfully executed'
        result3.task(":gradleTest").outcome == SUCCESS

        when: 'The task is executed again'
        final result4 = gradleRunner.build()

        then: 'the task is not executed again'
        result4.task(":gradleTest").outcome == UP_TO_DATE

        when: 'A single file is modified in the source set'
        deltaBuild << '''// Simple modifications to file'''
        final result5 = gradleRunner.build()

        then: 'the task will be successfully executed again'
        result5.task(":gradleTest").outcome == SUCCESS
    }

    void genSourceFiles() {
        final projSrcDir = new File(projectDir, 'src/main/java/test/ysb33r')
        projSrcDir.mkdirs()
        new File(projSrcDir, "${TEST_CLASS}.java").text = """
        package ${TEST_PKG};

        import org.gradle.api.Plugin;
        import org.gradle.api.Project;

        public class ${TEST_CLASS} implements Plugin<Project> {
            @Override
            public void apply(Project target) {
            }
        }
        """.stripIndent()
    }

    void genTestStructureForSuccess(File genToDir) {
        TESTNAMES.each {
            File testDir = new File(genToDir, it)
            testDir.mkdirs()
            new File(testDir, 'build.gradle').text = '''
            final String pName = project.name
            final String gVersion = gradle.gradleVersion
            // Use create so we can still test on 4.3.1
            tasks.create('runGradleTest')  {
                doLast {
                    println "I'm the runGradleTest task and I'm being executed from " + gVersion
                    println "Hello, ${pName}"
                }
            }
            '''.stripIndent()
        }
    }

    void genTestStructureForFailure() {
        TESTNAMES.each {
            File testDir = new File(srcDir, it)
            testDir.mkdirs()
            if (it == 'gamma') {
                new File(testDir, 'build.gradle').text = '''

            tasks.create( 'willFail' ) {
                doLast {
                    throw new GradleException('Expect this to fail')
                }
            }
            // Stick to create so that we can test on 4.3.1
            tasks.create( 'runGradleTest' ) {
                dependsOn 'willFail'
            }
            '''.stripIndent()

            } else {
                new File(testDir, 'build.gradle').text = '''
            final String pName = project.name
            final String gVersion = gradle.gradleVersion
            tasks.create ('runGradleTest')  {
                doLast {
                    println "I'm the runGradleTest task and I'm being executed from " + gVersion
                    println "Hello, ${pName}"
                }
            }
            '''.stripIndent()
            }
        }
    }

    void writeGradleTestConfiguration(String allVersions= allAvailableGradleReleasesAsDslString()) {
        buildFile << """
        gradlePlugin {
            plugins {
                aPlugin {
                    id = 'test.ysb33r.gradletest'
                    displayName = 'Plugin for compatibility testing of Gradle plugins'
                    implementationClass = '${TEST_PKG}.${TEST_CLASS}'
                }
            }
        }

        gradleTestSets {
            testSets {
                main {
                    versions ${allVersions}
                    gradleDistributionURI = '${GRADLETESTREPO.toURI().resolve('distributions')}'
                    deprecationMessagesAreFailuresForAllVersions = false
                }
            }
        }
        
        tasks.named('gradleTest') {
            final gVersion = gradle.gradleVersion
            doFirst {
                println 'I am the actual invocation of GradleTest (from GradleTestIntegrationSpec) and I am ' + gVersion
            }
        }
        """.stripIndent()
    }
}
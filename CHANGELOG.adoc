= CHANGELOG
:issue: https://gitlab.com/ysb33rOrg/gradleTest/issues/
:mr-url: https://gitlab.com/ysb33rOrg/gradleTest/merge_requests/


== 4.0.0 - 4.0.2

// tag::changelog[]

=== Features

* {issue}124[#124] - Let the compatibility tests automatically select the minimum supported version.
* {issue}150[#150] - GradleTest should prefer usage of `jvm-test-suite` plugin for creating tasks and configurations.

=== Bugs

* {issue}66[#66] - GradleTest tasks are skipped if no test classes folder to test task is present.
* {issue}76[#76] - File attributes are lost when copied during a test.
* {issue}102[#102] - External classpath not honored on rerun.
* {issue}132[#132] - Incorrect registration order for Test task and Generator task.
* {issue}134[#134] - Consecutive GradleTest execution fails if `.generated-src` directory is not deleted.
* {issue}135[#135] - GradleTest fails on `7.4.x`.
* {issue}138[#138] - GradleTest `--test` switch seems not working in version `3.0.0-alpha.3`.
* {issue}142[#142] - In `3.0.0-alpha.4` no `src/gradleTest` directory results in an exception.
* {issue}149[#149] - Custom TestSet does not automatically add `pluginUnderTestMetadata` to the runtime class path.
* {issue}153[#153] - Deprecation messages are not set correctly by version.
* {issue}154[#154] - When tests were generated, but command-line filter is applied, all tests are still run.
* {issue}155[#155] - Fix `.kts` being misspelt as `.kits`
* {issue}158[#158] - 'expectedFailures` does not generate test cases.
* {issue}159[#159] - Implementations for outdated test cases are not removed.
* {issue}160[#160] - Cannot use backslash in `deprecationWarnings` property.
* {issue}161[#161] - Cannot use backslash in `deprecationWarnings` property.

=== Other

* {issue}126[#126] - Added some compatibility tests that turn configuration cache on
* {issue}148[#148] - Uses Grolifant5 under the hood.
* {issue}149[#149] - Documentation now available on `gradletest.ysb33r.org`.
* {issue}156[#156] - Add note on `useCustomManifest` when `java-gradle-plugin` is not applied.

=== Breaking changes

* The GradleTest plugin applies the `jvm-test-suite` plugin.
  This might cause issues for some setups, which does not use this to configure test tasks.
* All configurations are now performed via `gradleTestSets.testSets` rather than on tasks.
* `expectFailures` renamed to `expectedFailures`.

=== Thank you

Special thanks goes to a number of people who contributed to releases prior to 4.0.
Without you, this would not be what it is today.

* https://github.com/dcendents[Daniel Beland] - Gradle 2.5 fixes.
* https://github.com/szpak[Marcin Zajączkowski] - Fix for Zip errors
* https://github.com/matthiasbalke[Matthias Balke] - Documentation
* https://github.com/scphantm[Willie Slepecki]:
** Multiple build files in one test project folder.
** Load distributions from enterprise repositories.

As to 4.0.x releases:

* Thanks goes to https://github.com/likemandrake[Piotr Minkina], who found some lacking documentation, after performing some user testing on 4.0.0, and then went on to spot {issue}155[#155], {issue}158[#158], {issue}159[#159], {issue}160[#160], {issue}161[#161].

// end::changelog[]

== 3.0.0 - 3.0.1

=== Features

* {issue}121[#121] - Support Gradle 7.x & 8.x
* {issue}128[#128] - Handle changes in format of deprecation warning messages.

=== Bugs

* {issue}118[#118] - Replace deprecated constructs.
* {issue}120[#120] - `externalDependencies` in `TestGenerator.exec()` should be created from `runtimeClasspath` configuration rather than `runtime` configuration.
* {issue}137[#137] - Correct cleanup of `build/` if the folder already exists for the version.
* {issue}147[#147] - Read JVM version from `GradleTest.javaLauncher`.

=== Breaking changes

* Maven coordinates have changed to use group `org.ysb33r.gradle`.
* `TestGenerator.getTestMap` returns a `Provider<Map<String, TestDefinition>>` instead of `TreeMap<String, TestDefinition>`.
* `TestGenerator.getTestRootDirectory` returns a `Provider<File>` instead of `File`.
* Minimum supported version to run GradleTest is 4.3. You can still test older versions.
* Upgraded to Grolifant 4.0.0.

=== Other

* {issue}125[#125] - Automated testing of the elimination of unsupported Gradle versions at JDK11, JDK14 boundaries.
* Correct handling of Jacoco registration for Gradle 8+



== 2.0

=== Features
* {issue}85[#85] - Allow for more than one build file per test project
* {issue}89[#89] - Allow for assigning a name pattern for the gradle distribution download
* {issue}97[#97] - When running with Gradle 4.5+ adding `--warning-mode=all` to command-line.
* {issue}98[#98] - `TestKit` work data can be captured for each test in a subfolder called `testkit-data`.
* {issue}107[#107] - Location of `TestKit` work data can be controlled per group of test or discarded after every test.

=== Breaking changes

* {issue}1[#1] - *Legacy mode removed*: This release will only support compatibility tests against Gradle 3.0 or later.

=== Bugs

* {issue}96[#96] - Add explicit import of `GradleVersion` to generated compatibility tests.
* {issue}108[#108] - JaCoCo not reporting coverage with gradleTest
* {issue}114[#114] - Use Spock Framework 1.2

=== Other

* {issue}104[#104] - Eliminate Gradle 5.0 warnings with regards to `classesDir` vs `classesDirs`.
* {issue}99[#99] - Remove debug printout from integration tests



== 1.1

=== Bugs

* {issue}76[#76] - File attributes are lost during copy.

== Breaking changes

* Now requires a minimum JDK7 irrespective of version Gradle. This means that if you run this version in legacy mode with an old version of Gradle you are still going to require JDK7.

== Roadmap 1.0

=== Features

* From Gradle 2.13+ GradleTestKit is used to run compatibility tests
* For Gradle 2.12 and earlier, legacy mode (from v0.5.5) is used for running tests.
* {issue}1[#1] - Graceful failure when tests have not passed
* {issue}2[#2] - HTML test report
* {issue}3[#3] - Running counter of tests being executed
* {issue}4[#4] - Capture test output and add to test report
* {issue}5[#5] - Allow tests to be executed in parallel
* {issue}52[#52] - Allow for expected failure cases
* {issue}54[#54] - Added base plugin `org.ysb33r.gradletest.base`.
* {issue}62[#62] - Propagate `--rerun-tasks` down to test instances.
* {issue}67[#67] - `org.ysb33r.gradlerunner` plugin provides `gradleRunner` task which can interleave different steps with independent Gradle executions.
* {issue}79[#79] - Make Gradle deprecation messages fail tests.
* {issue}82[#82] - Test Kotlin-based scripts if Gradle is 4.0+

=== Bugs

* {issue}26[#26] - Unable to build gradleTest project with Gradle 2.1+.
* {issue}34[#34] - Distribution URI fails with File paths under Windows.
* {issue}35[#35] - TestGenerator is not correctly substituting paths in
   initscript under Windows.
* {issue}38[#38] - TestGenerator is not correctly substituting paths in
   generated test source code under Windows.
* {issue}41[#41] - `gradleTest` configuration is broken.
* {issue}42[#42] - `GradleTest` HTML reports should not overwrite over Test
  task reports.
* {issue}45[#45] - When there is no `src/gradleTest` the build fails.
* {issue}46[#46] - When folders below `src/gradleTest` contain non-word characters,
  compilation of test classes fails.
* {issue}47[#47] - Classpath not correctly resolved under test.
* {issue}48[#48] - When copying gradleTest folders maintain same permissions
* {issue}49[#49] - When folders below `gradleTest` are removed, generated code
  should be removed too.
* {issue}53[#53] - Skip test generation if `src/gradleTest` does not exist.
* {issue}57[#57] - Transitive dependencies for pluginsare not injected into classpath.
* {issue}59[#59] - GradleTest does not detect new folders if previous task completed successfully
* {issue}74[#74] - Plugin resolves configuration `$$gradleRunner$$classpath$$` at configuration time.

=== Other

* {issue}12[#12] - Fixed Javadoc errors.
* {issue}36[#36] - Integration test for `GradleTest` failure under M$ Windows.
* {issue}37[#37] - Integration test for `legacy20.GradleTest` failure under
 M$ Windows.
* {issue}40[#40] - Added `gh-pages`-based .
* {issue}44[#44] - If license plugin is applied, exclude GradleTest sourcesets
    from check.
* {issue}50[#50] - Removed the use of deprecated `TaskInputs.source(Object)`
  when running under Gradle 3.0+.
* {issue}51[#51] - Compatibility test for multi-project scenarios.
* {issue}55[#55] - Memory: Split compatiblity tests into smaller runs on Appveyor.
* {issue}55[#56] - Memory: Use virtual image rather than container on TravisCI.
* {issue}60[#60] - Mention alternative approaches to `GradleTest`.
* {issue}61[#61] - Validate support for Gradle 3.3.
* {issue}63[#63] - Documented `--offline` behaviour.
* {issue}65[#65] - Documented CodeNarc plugin behaviour.
* {issue}78[#78] - Fix various deprecation messages with Gradle 4.x.


== Contributors

// tag::contributors[]

// end::contributors[]

== v0.5.5
* {issue}30[#30] - Specify init scripts
* {issue}28[#28], {mr-url}29[#29] - Better error
    message when ZIP archive is broken. (https://github.com/szpak[Marcin Zajączkowski])
* {mr-url}22[#22], {mr-url}23[#23] - Documentation
    corrections (https://github.com/matthiasbalke[Matthias Balke]).

== v0.5.4
* Updates for Gradle 2.5 

== v0.5.3
* {issue}13[#13] - Failure running against JDk6 or JDK7.

== v0.5.2
* {issue}11[#11] - Failure when GVM home directory does not exist.
* {issue}9[#9] - Unpacking dropped `-bin` off the folder name
* {issue}10[#10] - Distributions were incorrectly unpacked to a `gradleDist/gradleDist` folder.

== v0.5.1
* {issue}8[#8] - Fixed intermittent download issue

== v0.5
* Initial release
* Can test against various Gradle 2.x versions
* Very basic reporting

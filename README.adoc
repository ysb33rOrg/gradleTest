= GradleTest Gradle Plugin

Test your plugin against different versions of Gradle as part of your build. This plugin extends the power that `GradleTestKit` brings without having to actually author code.
It rather allows plugins authors to write functional tests that looks like normal Gradle projects (multi-project is supported).
This also makes it easier to create sample projects that can directly be used in documentation.

For full documentation see https://gradletest.ysb33r.org
